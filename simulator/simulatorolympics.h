#ifndef SIMULATOROLYMPICS_H
#define SIMULATOROLYMPICS_H

#include "simulatorstate.h"



class SimulatorOlympics : public SimulatorState
{
public:

    bool canGo();

    SimulatorOlympics();
    SimulatorOlympics(SimulatorState *);

    void createOlympics(QString, int, QString);
};

#endif // SIMULATOROLYMPICS_H
