#ifndef SIMULATOREMPTY_H
#define SIMULATOREMPTY_H

#include "simulatorstate.h"



class SimulatorEmpty : public SimulatorState
{
public:

    bool canGo();
    SimulatorEmpty();
    SimulatorEmpty(SimulatorState *);
    SimulatorEmpty(AbstractOlympicsBuilder *,
            AbstractSportFactory *,
            AbstractCompetitionFactory *,
            AbstractParticipantFactory *,
            AbstractSportsmanBuilder *);
};

#endif // SIMULATOREMPTY_H
