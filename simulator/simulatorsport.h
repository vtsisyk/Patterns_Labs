#ifndef SIMULATORSPORT_H
#define SIMULATORSPORT_H

#include "simulatorstate.h"



class SimulatorSport : public SimulatorState
{
public:
    SimulatorSport();
    SimulatorSport(SimulatorState *);
    bool canGo();
    void createSport(QString);
};

#endif // SIMULATORSPORT_H
