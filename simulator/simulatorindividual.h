#ifndef SIMULATORINDIVIDUAL_H
#define SIMULATORINDIVIDUAL_H

#include "simulatorstate.h"



class SimulatorIndividual : public SimulatorState
{
public:
    SimulatorIndividual();
    SimulatorIndividual(SimulatorState *);
    void createSportsman(QString name, QString country, int age, QString sex);
    bool canGo();
};

#endif // SIMULATORINDIVIDUAL_H
