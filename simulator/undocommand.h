#ifndef UNDOCOMMAND_H
#define UNDOCOMMAND_H

#include "abstractcommand.h"



class UndoCommand : public AbstractCommand
{
public:
    UndoCommand(Machine *machine);
    void execute();
};

#endif // UNDOCOMMAND_H
