#include "memento.h"



Memento::Memento(SimulatorState *_state)
{
    state = _state;
}

SimulatorState *Memento::restore()
{
    return state;
}
