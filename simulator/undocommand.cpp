#include "undocommand.h"

UndoCommand::UndoCommand(Machine *machine) : AbstractCommand(machine){}

void UndoCommand::execute()
{
    working->undo();
}
