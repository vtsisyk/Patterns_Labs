#ifndef MEMENTO_H
#define MEMENTO_H

class SimulatorState;
#include "simulatorstate.h"




class Memento
{
    SimulatorState * state;
public:
    Memento(SimulatorState *);
    SimulatorState *restore();

};

#endif // MEMENTO_H
