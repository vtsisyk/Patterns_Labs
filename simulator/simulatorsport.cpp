#include "simulatorsport.h"

SimulatorSport::SimulatorSport() : SimulatorState(){}

SimulatorSport::SimulatorSport(SimulatorState *state): SimulatorState(state){}

bool SimulatorSport::canGo()
{
    if(compFactory !=nullptr && currentSport !=nullptr)
        return true;
    return false;
}

void SimulatorSport::createSport(QString name)
{

    currentSport = sportFactory->createSport(name);
    currentGames->addSport(currentSport);
}
