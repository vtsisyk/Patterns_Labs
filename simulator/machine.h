#ifndef MACHINE_H
#define MACHINE_H

#include "simulatorstate.h"



class Machine
{
    SimulatorState *current;
    QVector<Memento *> states;
public:
    Machine();
    Machine(AbstractOlympicsBuilder *,
            AbstractSportFactory *,
            AbstractCompetitionFactory *,
            AbstractParticipantFactory *,
            AbstractSportsmanBuilder *);

     void createSport(QString);
     void createCompetition(QString, QString);
     void createSportsman(QString, QString, int, QString);
     void createOlympics(QString, int, QString);

     void setOlympicsBuilder(AbstractOlympicsBuilder *);
     void setSportFactory(AbstractSportFactory *);
     void setCompetitionFactory(AbstractCompetitionFactory *);
     void setSportsmanBuilder(AbstractSportsmanBuilder *);
     void setParticipantsFactory(AbstractParticipantFactory *);

     SimulatorState *getCurrent();


      AbstractOlympics * getcurrentGames(){return current->getcurrentGames();}
      AbstractSport * getcurrentSport(){return current->getcurrentSport();}
      AbstractCompetition *getcurrentCompetition(){return current->getcurrentCompetition();}
      Participant *getcurrentParticipant(){return current->getcurrentParticipant();}

      void setcurrentGames(AbstractOlympics *games){current->setcurrentGames(games);}
      void setcurrentSport(AbstractSport *sport){current->setcurrentSport(sport);}
      void setcurrentCompetition(AbstractCompetition *comp){current->setcurrentCompetition(comp);}
      void setcurrentParticipant(Participant *part){current->setcurrentParticipant(part);}


     AbstractOlympics *getOlympics();
    void setCurrent(SimulatorState *s);
    void nextStep();

    void undo();
};

#endif // MACHINE_H
