#include "simulatorcompetition.h"

SimulatorCompetition::SimulatorCompetition() : SimulatorState(){}

SimulatorCompetition::SimulatorCompetition(SimulatorState *state) : SimulatorState(state){}

void SimulatorCompetition::createCompetition(QString name, QString type)
{

    if(type.toLower() == "individual"){
        currentCompetition = compFactory->createIndividual(name);
        whichnext = true;
    }
    if(type.toLower() == "collective"){
        currentCompetition = compFactory->createCollective(name);
        whichnext = false;
    }

    currentSport->addCompetition(currentCompetition);
}

bool SimulatorCompetition::canGo()
{
    if(partBuilder != nullptr && personBuilder !=nullptr && currentCompetition !=nullptr)
        return true;
    return false;
}

bool SimulatorCompetition::next()
{
    return whichnext;
}
