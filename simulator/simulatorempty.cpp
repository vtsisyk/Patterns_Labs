#include "simulatorempty.h"



bool SimulatorEmpty::canGo()
{
    if(gamesBuilder != nullptr)
        return true;
    return false;
}

SimulatorEmpty::SimulatorEmpty() : SimulatorState(){}

SimulatorEmpty::SimulatorEmpty(SimulatorState *state) : SimulatorState(){}

SimulatorEmpty::SimulatorEmpty(AbstractOlympicsBuilder *games,
                               AbstractSportFactory *sport,
                               AbstractCompetitionFactory *comp,
                               AbstractParticipantFactory *part,
                               AbstractSportsmanBuilder *pers) :
    SimulatorState (games, sport, comp, part, pers)
{

}

