#include "simulatorindividual.h"

SimulatorIndividual::SimulatorIndividual() : SimulatorState(){}

SimulatorIndividual::SimulatorIndividual(SimulatorState *state): SimulatorState(state){}

void SimulatorIndividual::createSportsman(QString name,
                                          QString country,
                                          int age,
                                          QString sex)
{
     personBuilder->createSportsman(name, country, age, sex);

     partBuilder->createParticipant(personBuilder->getSportsman(), currentSport->getTitle());
     int dope = (QRandomGenerator::global()->bounded(20));

     if(dope > 16)
        partBuilder->dope();
     dope = (QRandomGenerator::global()->bounded(20));
     if(dope > 16)
         partBuilder->injury();




     Participant *tmp = partBuilder->getParticipant();
     currentParticipant = tmp;
     tmp->setstatus("Участник");
     tmp->setplace(QRandomGenerator::global()->bounded(5));



    ((IndividualCompetition *) currentCompetition)->addParticipant(tmp);


}

bool SimulatorIndividual::canGo()
{
    return true;
}
