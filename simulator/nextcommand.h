#ifndef NEXTCOMMAND_H
#define NEXTCOMMAND_H

#include "abstractcommand.h"



class NextCommand : public AbstractCommand
{
public:
    NextCommand(Machine *);
    void execute();
};

#endif // NEXTCOMMAND_H
