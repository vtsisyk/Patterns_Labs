#ifndef SIMULATORSTATE_H
#define SIMULATORSTATE_H

class Memento;
#include "memento.h"

#include <QDebug>
#include <abstractcompetitionfactory.h>
#include <abstractolympicsbuilder.h>
#include <abstractparticipantfactory.h>
#include <abstractsportfactory.h>
#include <abstractsportsmanbuilder.h>
class SimulatorState
{
protected:
    AbstractOlympics * currentGames;
    AbstractSport * currentSport;
    AbstractCompetition *currentCompetition;
    Participant *currentParticipant;

    AbstractSportFactory *sportFactory;
    AbstractCompetitionFactory *compFactory;
    AbstractParticipantFactory *partBuilder;
    AbstractSportsmanBuilder *personBuilder;


    AbstractOlympicsBuilder *gamesBuilder;
public:

    virtual AbstractOlympics * getcurrentGames(){return currentGames;}
    virtual AbstractSport * getcurrentSport(){return currentSport;}
    virtual AbstractCompetition *getcurrentCompetition(){return currentCompetition;}
    virtual Participant *getcurrentParticipant(){return currentParticipant;}

    virtual void createSport(QString);
    virtual void createCompetition(QString, QString);
    virtual void createSportsman(QString, QString, int, QString);
    virtual void createOlympics(QString, int, QString);

    virtual void setOlympicsBuilder(AbstractOlympicsBuilder *);
    virtual void setSportFactory(AbstractSportFactory *);
    virtual void setCompetitionFactory(AbstractCompetitionFactory *);
    virtual void setSportsmanBuilder(AbstractSportsmanBuilder *);
    virtual void setParticipantsFactory(AbstractParticipantFactory *);


    virtual void setcurrentGames(AbstractOlympics *games);
    virtual void setcurrentSport(AbstractSport *sport);
    virtual void setcurrentCompetition(AbstractCompetition *comp);
    virtual void setcurrentParticipant(Participant *part);

    virtual bool canGo() = 0;

    virtual AbstractOlympics *getOlympics();


    SimulatorState();
    SimulatorState(SimulatorState *);

    SimulatorState(AbstractOlympicsBuilder *,
            AbstractSportFactory *,
            AbstractCompetitionFactory *,
            AbstractParticipantFactory *,
            AbstractSportsmanBuilder *);

    virtual Memento *createMemento();

};

#endif // SIMULATORSTATE_H
