#include "simulatorolympics.h"


bool SimulatorOlympics::canGo()
{
    if(sportFactory !=nullptr && currentGames !=nullptr)
        return true;
    return false;
}

SimulatorOlympics::SimulatorOlympics(SimulatorState * state) : SimulatorState(state){}

void SimulatorOlympics::createOlympics(QString name, int year, QString type)
{
    gamesBuilder->createOlympics(name, year, type);
    currentGames = gamesBuilder->getOlympics();
}

