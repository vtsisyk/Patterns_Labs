#include "simulatorstate.h"

void SimulatorState::createSport(QString){}

void SimulatorState::createCompetition(QString, QString){}

void SimulatorState::createSportsman(QString, QString, int, QString){}

void SimulatorState::createOlympics(QString, int, QString){}

void SimulatorState::setOlympicsBuilder(AbstractOlympicsBuilder *builder)
{
    gamesBuilder = builder;
}

void SimulatorState::setSportsmanBuilder(AbstractSportsmanBuilder *builder)
{
    personBuilder = builder;
}

void SimulatorState::setParticipantsFactory(AbstractParticipantFactory *factory)
{
    partBuilder = factory;
}

void SimulatorState::setcurrentGames(AbstractOlympics *games){currentGames = games;}

void SimulatorState::setcurrentSport(AbstractSport *sport){currentSport = sport;}

void SimulatorState::setcurrentCompetition(AbstractCompetition *comp){currentCompetition = comp;}

void SimulatorState::setcurrentParticipant(Participant *part){currentParticipant = part;}

AbstractOlympics *SimulatorState::getOlympics()
{
    if(currentGames != nullptr)
        return currentGames;
}

void SimulatorState::setSportFactory(AbstractSportFactory *factory)
{
    sportFactory = factory;
}

void SimulatorState::setCompetitionFactory(AbstractCompetitionFactory *factory)
{
    compFactory = factory;
}

SimulatorState::SimulatorState()
{
    sportFactory =  nullptr;
    compFactory = nullptr;
    partBuilder =  nullptr;
    personBuilder = nullptr;
    gamesBuilder = nullptr;

    currentGames = nullptr;
    currentSport = nullptr;
    currentCompetition = nullptr;

}

SimulatorState::SimulatorState(SimulatorState *state)
{
    if(state->gamesBuilder !=  nullptr)
        gamesBuilder =  state->gamesBuilder;

    if(state->sportFactory !=  nullptr)
        sportFactory =  state->sportFactory;

    if(state->compFactory !=  nullptr)
        compFactory =  state->compFactory;

    if(state->partBuilder !=  nullptr)
        partBuilder =  state->partBuilder;

    if(state->personBuilder !=  nullptr)
        personBuilder =  state->personBuilder;

    if(state->currentGames !=  nullptr)
        currentGames =  state->currentGames;

    if(state->currentSport !=  nullptr)
        currentSport =  state->currentSport;

    if(state->currentCompetition !=  nullptr)
        currentCompetition = state->currentCompetition;
}

SimulatorState::SimulatorState(AbstractOlympicsBuilder *games,
                               AbstractSportFactory *sport,
                               AbstractCompetitionFactory *comp,
                               AbstractParticipantFactory *part,
                               AbstractSportsmanBuilder *pers)
{


    sportFactory =  nullptr;
    compFactory = nullptr;
    partBuilder =  nullptr;
    personBuilder = nullptr;
    gamesBuilder = nullptr;

    currentGames = nullptr;
    currentSport = nullptr;
    currentCompetition = nullptr;

    setOlympicsBuilder(games);
    setSportFactory(sport);
    setCompetitionFactory(comp);
    setParticipantsFactory(part);
    setSportsmanBuilder(pers);
}

Memento *SimulatorState::createMemento()
{
    return new Memento(this);
}
