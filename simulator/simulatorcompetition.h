#ifndef SIMULATORCOMPETITION_H
#define SIMULATORCOMPETITION_H

#include "simulatorstate.h"



class SimulatorCompetition : public SimulatorState
{
    // false - collective, true - inidvidual;
    bool whichnext = true;
public:
    SimulatorCompetition();
    SimulatorCompetition(SimulatorState *state);

    void createCompetition(QString, QString);
    bool canGo();

    bool next();
};

#endif // SIMULATORCOMPETITION_H
