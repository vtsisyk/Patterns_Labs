#ifndef SIMULATORPARTICIPANT_H
#define SIMULATORPARTICIPANT_H

#include "simulatorstate.h"



class SimulatorParticipant : public SimulatorState
{
public:
    SimulatorParticipant();
    SimulatorParticipant(SimulatorState *);
};

#endif // SIMULATORPARTICIPANT_H
