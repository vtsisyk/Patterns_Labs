#ifndef ABSTRACTCOMMAND_H
#define ABSTRACTCOMMAND_H

#include "machine.h"



class AbstractCommand
{
protected:
    Machine *working;
public:
    AbstractCommand(Machine *);
    virtual void execute() = 0;

};

#endif // ABSTRACTCOMMAND_H
