#include "machine.h"
#include "simulatorcompetition.h"
#include "simulatorempty.h"
#include "simulatorindividual.h"
#include "simulatorolympics.h"
#include "simulatorparticipant.h"
#include "simulatorsport.h"

Machine::Machine()
{
    current = new SimulatorEmpty();
}

Machine::Machine(AbstractOlympicsBuilder *games,
                 AbstractSportFactory *sport,
                 AbstractCompetitionFactory *comp,
                 AbstractParticipantFactory *part,
                 AbstractSportsmanBuilder *pers)
{
    current = new SimulatorEmpty(games, sport, comp, part, pers);
}

void Machine::createSport(QString sport)
{
    current->createSport(sport);
}

void Machine::createCompetition(QString name, QString type)
{
    current->createCompetition(name, type);
}

void Machine::createSportsman(QString name, QString country, int age, QString sex)
{
    current->createSportsman(name, country, age, sex);
}

void Machine::createOlympics(QString name, int year, QString type)
{
    current->createOlympics(name, year, type);
}

void Machine::setOlympicsBuilder(AbstractOlympicsBuilder *builder)
{
    current->setOlympicsBuilder(builder);
}

void Machine::setSportFactory(AbstractSportFactory *factory)
{
    current->setSportFactory(factory);
}

void Machine::setCompetitionFactory(AbstractCompetitionFactory *factory)
{
    current->setCompetitionFactory(factory);
}

void Machine::setSportsmanBuilder(AbstractSportsmanBuilder * builder)
{
    current->setSportsmanBuilder(builder);
}

void Machine::setParticipantsFactory(AbstractParticipantFactory *factory)
{
    current->setParticipantsFactory(factory);
}

AbstractOlympics *Machine::getOlympics()
{
    return current->getOlympics();
}


void Machine::setCurrent(SimulatorState *s)
{
    current = s;
}

void Machine::nextStep()
{
    if(dynamic_cast<SimulatorEmpty *>(current) != nullptr){
        if(current->canGo()){
            states.push_back(current->createMemento());
            current = new SimulatorOlympics(current);
        }
    } else if(dynamic_cast<SimulatorCompetition *>(current) != nullptr){
        SimulatorCompetition *tmp = (SimulatorCompetition *) current;
        if(current->canGo()){
            states.push_back(current->createMemento());
            if(tmp->next())
                current = new SimulatorIndividual(current);
        }
    } else if(dynamic_cast<SimulatorIndividual *>(current) != nullptr){
        if(current->canGo()){
            states.push_back(current->createMemento());
            current = new SimulatorSport(current);
        }
    } else if(dynamic_cast<SimulatorOlympics *>(current) != nullptr){
        if(current->canGo()){
            states.push_back(current->createMemento());
            current = new SimulatorSport(current);
        }
    } else if(dynamic_cast<SimulatorParticipant *>(current) != nullptr){
        if(current->canGo()){
            states.push_back(current->createMemento());
            current = new SimulatorOlympics(current);
        }
    } else if(dynamic_cast<SimulatorSport *>(current) != nullptr){
        if(current->canGo()){
            states.push_back(current->createMemento());
            current = new SimulatorCompetition(current);
        }
    }

}

void Machine::undo()
{
    current = states.last()->restore();
    delete states.last();
    states.pop_back();
}
