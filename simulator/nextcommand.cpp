#include "nextcommand.h"

NextCommand::NextCommand(Machine * machine) : AbstractCommand(machine){}

void NextCommand::execute()
{
    working->nextStep();
}
