#ifndef NULLPARTICIPANT_H
#define NULLPARTICIPANT_H

#include "participant.h"



class NullParticipant : public Participant
{
public:
  static NullParticipant *Instance();

private:

  NullParticipant();  // конструктор недоступен
  ~NullParticipant(); // и деструктор
  QString getresult();
  Participant *clone();

  // необходимо также запретить копирование
  NullParticipant(NullParticipant const&); // реализация не нужна
  NullParticipant& operator= (NullParticipant const&);  // и тут
};

#endif // NULLPARTICIPANT_H
