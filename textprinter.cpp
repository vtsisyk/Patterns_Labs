#include "individualcompetition.h"
#include "textprinter.h"
#include <QSet>;


TextPrinter::TextPrinter(AbstractOlympics *games)
{
    model = games;
    buf = std::cout.rdbuf();
    out = new std::ostream(buf);
}

void TextPrinter::notify()
{
    printOlympics();
}
TextPrinter::TextPrinter(AbstractOlympics *games, QString file)
{
    model = games;
    of.open(file.toStdString());
    buf = of.rdbuf();
    out = new std::ostream(buf);
}

void TextPrinter::printOlympics()
{
    *out <<  "\n";
    *out << "--------------------Игры------------------------" << "\n";
    *out << "название: " << model->getName().toStdString() << "\n";
    *out << "год: " << model->getYear() << "\n";
    *out << "вид игр: " << model->getType().toStdString() << "\n";
    *out << "------------------------------------------------" << "\n";
    *out <<  "\n";

    for(AbstractSport *sport: model->getSports())
        printSports(sport);
}

void TextPrinter::printSports(AbstractSport *sport)
{
    *out <<  "\n";
    *out << "--------------------";
    *out << sport->getTitle().toStdString();
    *out << "--------------------";
    *out <<  "\n";
    for(AbstractCompetition *comp: sport->getCompetitions())
        printCompetitions(comp);

}

void TextPrinter::printCompetitions(AbstractCompetition *comp)
{
    *out <<  "\n";
    *out << "--------------------";
    *out << comp->getTitle().toStdString();
    *out << "--------------------";
    *out <<  "\n";
    for(Participant *part: comp->listParticipants())
        printParticipants(part);


}

void TextPrinter::printParticipants(Participant *p)
{
    *out <<  "\n";
    *out << "====================";
    *out <<  "\n";
    *out << "Имя:" << p->getname().toStdString() << "\n";
    *out << "Страна:" << p->getcountry().toStdString() << "\n";
    *out << "Возраст:" << QString::number(p->getAge()).toStdString() << "\n";
    *out << "Пол:" << p->getSex().toStdString() << "\n";
    *out << "Статус:" << p->getstatus().toStdString() << "\n";
    *out << "====================";
    *out <<  "\n";


}


void TextPrinter::setOutFile(QString)
{
    of.open("file.txt");
    buf = of.rdbuf();
}
