#include "sportsman.h"

Sportsman::Sportsman(QString _name,
                     QString _country,
                     unsigned int _age,
                     QString _sex)
{
    name = _name;
    country = _country;
    age = _age;
    sex = _sex;

    custompic = false;
}

Sportsman::~Sportsman()
{
    if(custompic)
        delete image;
}
Sportsman::Sportsman()
{

}

QString Sportsman::getname()
{
    return name;
}

QString Sportsman::getcountry()
{
    return country;
}

QString Sportsman::getSex()
{
    return sex;
}

unsigned int Sportsman::getAge()
{
    return age;
}

QImage *Sportsman::getImage()
{
    return image;
}

void Sportsman::setimage(QImage img)
{
    if(custompic)
        delete image;

    image = new QImage(img);
    custompic = true;

}

void Sportsman::setimage(QImage *img)
{
    if(custompic)
        delete image;

    image = img;
    custompic = false;
}
