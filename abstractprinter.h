#ifndef ABSTRACTPRINTER_H
#define ABSTRACTPRINTER_H
class AbstractOlympics;
#include "abstractolympics.h"


class AbstractPrinter
{
protected:
    AbstractOlympics *model;
public:
      virtual void notify() = 0;

};

#endif // ABSTRACTPRINTER_H
