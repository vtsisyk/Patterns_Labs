

#include "mainwindow.h"
#include <QFileDialog>
#include "abstractcompetitionfactory.h"
#include "abstractparticipantfactory.h"
#include "abstractprinter.h"
#include "competitionfactory.h"
#include "individualcompetition.h"
#include "olympicsbuilder.h"
#include "section.h"
#include "sport.h"
#include "sportfactory.h"
#include "sportsmanform.h"
#include "team.h"
#include "textprinter.h"
#include "ui_mainwindow.h"
#include "winterparticipantfactory.h"

#include <QGraphicsPixmapItem>
#include <random>
#include <QDebug>
#include <QPropertyAnimation>
#include <QObject>
#include <QPushButton>


#include <QVBoxLayout>
#include <sportsman.h>
#include <sportsmanbuilder.h>

#include <sports/figureskater.h>
#include <sports/skier.h>
#include <olympics.h>
#include <decorators/dopedecorator.h>
#include <decorators/injurydecorator.h>
#include <QComboBox>
#include <editui/sportsform.h>

#include <QStandardItemModel>


void MainWindow::printSports()
{

    sportmodel = new QStandardItemModel(this);

    ui->sportsListView->setModel(sportmodel);
    for(AbstractSport * sport : model->getSports()){
        sportmodel->appendRow(new QStandardItem(*(sport->getImage()), sport->getTitle()));

    }




    clear();
    ui->verticalSpacer->changeSize(0,0,QSizePolicy::Minimum,QSizePolicy::Expanding);
    drawPieChart(model->personsByCountry(), "Участники по странам", "количество медалей у ", ui->upper);

    drawBarChart(model->personsByCountry(), "Количество участников по странам", ui->lower);

}

void MainWindow::printCompetitions(int pos)
{
    compmodel = new QStandardItemModel(this);

    ui->verticalSpacer->changeSize(0,0,QSizePolicy::Minimum,QSizePolicy::Expanding);
    ui->competitionListView->setModel(compmodel);
    for(AbstractCompetition * sport : model->getSports()[pos]->getCompetitions()){
        compmodel->appendRow(new QStandardItem(sport->getTitle()));

    }


    clear();
    // ui->verticalSpacer->changeSize(0,0,QSizePolicy::Minimum,QSizePolicy::Preferred);
    drawPieChart(model->getSports()[pos]->personsByCountry(),
                 "Участники в " + model->getSports()[pos]->getTitle(),
                 "количество медалей в этом виде спорта у  ", ui->upper);

    drawBarChart(model->getSports()[pos]->personsByCountry(), "Страны-призеры", ui->lower);


}



void MainWindow::drawBarChart(QMap<QString, countryinfo> info, QString title, QVBoxLayout *layout)
{
    QStringList categories;
    QBarSeries *series = new QBarSeries();

    QMap<QString, countryinfo>::iterator it;
    for (it = info.begin(); it != info.end(); ++it) {
        int sum = it.value().gold +  it.value().silver + it.value().bronze;
        if(sum > 0){
            QBarSet *set0 = new QBarSet(it.key());
            *set0 << it.value().gold +  it.value().silver + it.value().bronze;
            series->append(set0);
            categories.append(it.key());
        }
    }

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle(title);
    chart->setAnimationOptions(QChart::SeriesAnimations);


    QBarCategoryAxis *axis = new QBarCategoryAxis();

    axis->append(categories);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    lower = new QChartView(chart);
    lower->setRenderHint(QPainter::Antialiasing);

    ui->lower->addWidget(lower);
    lower->resize(420, 300);
    lower->show();


}

void MainWindow::addMachine(Machine *m)
{
//    ui->addSportButton->setEnabled(true);
    machine = m;
}


void MainWindow::drawPieChart(QMap<QString, countryinfo> info, QString title1, QString title2, QVBoxLayout *layout)
{


    QStringList persons;
    QStringList countries;

    //QMap<QString, countryinfo> info = model->personsByCountry();
    QMap<QString, countryinfo>::iterator it;

    int sum = 0;
    for (it = info.begin(); it != info.end(); ++it) {
        countries.push_back(it.key());
        persons.push_back(QString::number(it.value().participants));
        sum += it.value().participants;
    }

    CustomPieChart *chart = new CustomPieChart();
    chart->setTheme(QChart::ChartThemeLight);
    chart->setAnimationOptions(QChart::AllAnimations);
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignRight);

    QPieSeries *subSeries = new QPieSeries();
    subSeries->setName(title1);

    for (it = info.begin(); it != info.end(); ++it) {
        QPieSeries *series = new QPieSeries();
        series->setName(title2 + it.key());

        CustomSlice *tmp;

        if(it.value().gold > 0){
            tmp = new CustomSlice(it.value().gold, "Золото("+ QString::number(it.value().gold) + ")", subSeries);
            tmp->setColor(QColor("#ffd700"));
            *series << tmp;
        }


        if(it.value().silver > 0){
            tmp = new CustomSlice(it.value().silver, "Серебро(" + QString::number(it.value().silver) + ")", subSeries);
            tmp->setColor(QColor("#c0c0c0"));
            *series << tmp;
        }

        if(it.value().bronze > 0){
            tmp = new CustomSlice(it.value().bronze, "Бронза("+ QString::number(it.value().bronze) + ")", subSeries);
            tmp->setColor(QColor("#CD7F32"));
            *series << tmp;
        }

        if(it.value().gold == 0 && it.value().silver == 0 && it.value().bronze == 0 ){
            tmp = new CustomSlice(1, "Ничего :(", subSeries);
            tmp->setColor(QColor("#084FDD"));
            *series << tmp;
        }


        QObject::connect(series, &QPieSeries::clicked, chart, &CustomPieChart::handleSliceClicked);

        *subSeries << new CustomSlice(it.value().participants,
                                      it.key() +
                                      "("+ QString::number(it.value().participants)+")"
                                      , series);
    }

    QObject::connect(subSeries, &QPieSeries::clicked, chart, &CustomPieChart::handleSliceClicked);

    chart->changeSeries(subSeries);

    upper = new QChartView(chart);
    upper->setRenderHint(QPainter::Antialiasing);

    layout->addWidget(upper);
    // ui->upper->addWidget(chartView);
    upper->setMinimumWidth(400);
    // upper->setMinimumHeight(400);
    // view->resize(800, 500);
    upper->show();

}
void MainWindow::notify()
{
    printSports();
    this->show();

}

MainWindow::MainWindow(AbstractOlympics *game, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    model = game;
    lower = nullptr;
    upper = nullptr;
    scroll = nullptr;


    model->addPrinter(this);
    ui->setupUi(this);


   // section = new Section("Section", 200, ui->centralWidget);
   // ui->bottomLayout->addWidget(section);

    sportform = new SportsForm();

    auto* anyLayout = new QVBoxLayout();

     anyLayout->addWidget(sportform);
    //anyLayout->addWidget(new QPushButton("Button in Section", section));

   // section->setContentLayout(*anyLayout);
    // ui->centralWidget->layout()->addWidget(section);

    ui->OlympicsNameLabel->setText(model->getName() + " "+ QString::number(model->getYear())  );
    this->hide();
    connect(ui->sportsListView, SIGNAL(clicked(QModelIndex)), this, SLOT(selectedSport(QModelIndex)));
    connect(ui->competitionListView, SIGNAL(clicked(QModelIndex)), this, SLOT(selectedCompetition(QModelIndex)));

    connect(ui->addSportButton, SIGNAL(clicked(bool)), this, SLOT(addSport()));
    connect(ui->rmSportButton, SIGNAL(clicked(bool)), this, SLOT(rmSport()));

    connect(ui->addCompetitionButton, SIGNAL(clicked(bool)), this, SLOT(addCompetition()));
    connect(ui->rmCompetitionButton, SIGNAL(clicked(bool)), this, SLOT(rmCompetition()));
    connect(ui->save, SIGNAL(triggered(bool)), this, SLOT(save()));

    connect(ui->olympicsInfoButton, SIGNAL(clicked(bool)), this, SLOT(printSports()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addSport()
{
   // sportmodel->appendRow(new QStandardItem("nic tu nie ma"));
    machine->createSport("Новый Спорт");
    model->notify();
}

void MainWindow::rmSport()
{
    model->rmSport(model->getSports()[sports]);
    model->notify();
}

void MainWindow::rmCompetition()
{
    model->getSports()[sports]->rmCompetition(model->getSports()[sports]->getCompetitions()[competition]);

    printCompetitions(sports);
    model->notify();
}

void MainWindow::addCompetition()
{
    machine->setcurrentSport(model->getSports()[sports]);
    machine->createCompetition("Новое соревнование", "individual");

    delete compmodel;
    model->notify();
}

void MainWindow::selectedSport(QModelIndex pos)
{

    QStandardItem *it = sportmodel->itemFromIndex(pos);
   // section->setTitle(it->text());
    printCompetitions(pos.row());
    sports = pos.row();
        sportform->setData(model->getSports()[sports]);
}

void MainWindow::selectedCompetition(QModelIndex pos)
{


    clear();
    competition = pos.row();
    ui->verticalSpacer->changeSize(0,0,QSizePolicy::Minimum,QSizePolicy::Minimum);
    //upper->hide();

    AbstractCompetition *tmp = model->getSports()[sports]->getCompetitions()[pos.row()];


//        section->setTitle(model->getSports()[sports]->getTitle() + ": " + tmp->getTitle());

    QVector<Participant *> winners = tmp->getWinners();


    QWidget *main = new QWidget();
    scroll = new QScrollArea();
    scroll->setWidgetResizable(true);
    scroll->setWidget(main);

        QVBoxLayout *vbox = new QVBoxLayout();
    main->setLayout(vbox);

    for(Participant *p: winners){

        SportsmanForm * sportsmanForm= new SportsmanForm(p);

   //     connect(sportsmanForm, SIGNAL(removeThis(SportsmanForm *)), this, SLOT(rmparticipant(SportsmanForm*));
        QObject::connect(sportsmanForm, &SportsmanForm::removeThis, [this](SportsmanForm * s)
        {
    model->getSports()[sports]->getCompetitions()[competition]->rmParticipant(s->getParticipant());

            delete s;
            /* удалить их из выбранного соревнования  */
        });
        vbox->addWidget(sportsmanForm);
    }


   // spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding);
   // vbox->addItem(spacer);
    ui->lower->addWidget(scroll);
  //  ui->lower->addSpacerItem(spacer);

}

void MainWindow::save()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Сохранить в файл"), "",
                                                    tr("Текстовый файл (*.txt);;All Files (*)"));

    if(!fileName.isEmpty()){
        TextPrinter tmp(model, fileName);
        tmp.printOlympics();
        model->rmPrinter(&tmp);
    }

}

void MainWindow::clear()
{
    if(upper !=nullptr){
        delete upper;
        upper = nullptr;
    }
    if(lower !=nullptr){
        delete lower;
        lower = nullptr;
    }
    if(scroll != nullptr){
        delete scroll;
        scroll = nullptr;
    }

}
