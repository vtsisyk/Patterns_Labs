#ifndef DEFAULTIMAGESPOOL_H
#define DEFAULTIMAGESPOOL_H

#include <QIcon>
#include <QImage>



class DefaultImagesPool //: public AbstractPool
{
public:
   static DefaultImagesPool& Instance()
   {
       // согласно стандарту, этот код ленивый и потокобезопасный
       static DefaultImagesPool s;
       return s;
   }


   void setUserpic(QImage);
   QImage *getUserpic();

   void setSkipic(QIcon);
   QIcon *getSkipic();


   QIcon *getRings();

   QIcon *sportIcon(QString);

   //void load(QPath);
 private:
   QImage userpic;

   QIcon hockeypic;
   QIcon biathlonpic;
   QIcon swimpic;
   QIcon skipic;
   QIcon logo;
   QIcon rings;

   DefaultImagesPool();  // конструктор недоступен
   ~DefaultImagesPool() { } // и деструктор

   // необходимо также запретить копирование
   DefaultImagesPool(DefaultImagesPool const&); // реализация не нужна
   DefaultImagesPool& operator= (DefaultImagesPool const&);  // и тут
};

#endif // DEFAULTIMAGESPOOL_H
