#ifndef OLYMPICS_H
#define OLYMPICS_H

#include "abstractolympics.h"
#include "sport.h"
#include "sportsmanpool.h"

#include <QtCore/QRandomGenerator>
#include <QVector>



class Olympics : public AbstractOlympics
{
private:
    QString name;
    int year;
    QString type;
    QVector<AbstractSport *> sports;
    QVector<AbstractPrinter *> printers;

    SportsmanPool pool;

public:
    void addPerson(AbstractSportsman *);
    void rmSportsman(AbstractSportsman *);

    void rmSport(AbstractSport *);


    bool sportsmanExists(AbstractSportsman *);

    QSet<Participant *> listParticipants();

    void addPrinter(AbstractPrinter *);
    void rmPrinter(AbstractPrinter *);
    void rmLastPrinter();
    QString getName();
    QString getType();
    void addSport(AbstractSport *);
    QVector<AbstractSport *> getSports();
    Olympics(QString, int, QString);
    int getYear() const;
    void setYear(int value);

    QMap<QString, countryinfo > personsByCountry();

    void notify();
};

#endif // OLYMPICS_H
