#ifndef SPORT_H
#define SPORT_H

#include "abstractsport.h"
#include <QVector>
#include <abstractcompetition.h>

class Sport : public AbstractSport
{
private:
    QString title;
    QIcon *img;
    QVector<AbstractCompetition *> competitions;


public:
    Sport(QString, QIcon *);
    Sport(Sport *);
    QString getTitle();
    void setTitle(QString _title);
    QIcon *getImage();
    void setImage(QIcon *);

    void addCompetition(AbstractCompetition *);

    void rmCompetition(AbstractCompetition *);
    QVector<AbstractCompetition *> getCompetitions();
    bool sportsmanExists(AbstractSportsman *);


    QSet<Participant *> listParticipants();

    QMap<QString, countryinfo > personsByCountry();

    AbstractSport *clone();

    ~Sport();


};

#endif // SPORT_H
