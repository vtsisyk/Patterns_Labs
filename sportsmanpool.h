#ifndef SPORTSMANPOOL_H
#define SPORTSMANPOOL_H

#include "abstractsportsman.h"

#include <QSet>



class SportsmanPool
{
    QSet<AbstractSportsman *> persons;
public:
    SportsmanPool();
    void addSportsman(AbstractSportsman *);
    void rmSportsman(AbstractSportsman *);
    QSet<AbstractSportsman *> getSportsmans();
};

#endif // SPORTSMANPOOL_H
