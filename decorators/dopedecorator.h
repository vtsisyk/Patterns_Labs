#ifndef DOPEDECORATOR_H
#define DOPEDECORATOR_H

#include "basedecorator.h"



class DopeDecorator : public BaseDecorator
{
public:
    QString getstatus();
    QString getInfo();
    DopeDecorator(Participant *, QString);
    DopeDecorator(Participant *);
    Participant *clone();
    DopeDecorator(DopeDecorator *);
};

#endif // DOPEDECORATOR_H
