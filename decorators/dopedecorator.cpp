#include "dopedecorator.h"

QString DopeDecorator::getstatus()
{
   // return status + getInfo();

     return  part->getstatus() + getInfo();
}

QString DopeDecorator::getInfo()
{
    return additional_info ;
}

Participant *DopeDecorator::clone()
{
    return new DopeDecorator(this);
}

DopeDecorator::DopeDecorator(DopeDecorator *dope) : BaseDecorator(dope){


}
DopeDecorator::DopeDecorator(Participant *s, QString str) : BaseDecorator(s, str){}

DopeDecorator::DopeDecorator(Participant *participant)
{
    part = participant;
    additional_info = " допинг";
}


