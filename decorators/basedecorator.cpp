#include "basedecorator.h"




QString BaseDecorator::getresult()
{
    return part->getstatus();
}

int BaseDecorator::getplace()
{
    return part->getplace();
}

void BaseDecorator::setplace(int _place)
{
    part->setplace(_place);
}

QString BaseDecorator::getname()
{
    return part->getname();
}

QString BaseDecorator::getcountry()
{
    return part->getcountry();
}

void BaseDecorator::setimage(QImage img)
{
    return part->setimage(img);
}

void BaseDecorator::setimage(QImage *img)
{
    return part->setimage(img);
}

QString BaseDecorator::getstatus()
{
    return part->getresult();
}

void BaseDecorator::setstatus(QString stat)
{
    part->setstatus(stat);
}

QString BaseDecorator::getSex()
{
    return part->getSex();
}

unsigned int BaseDecorator::getAge()
{
    return part->getAge();
}

QImage *BaseDecorator::getImage()
{
    return part->getImage();
}

Participant *BaseDecorator::clone(){return this;}

BaseDecorator::BaseDecorator(BaseDecorator * dec)
{

    part = dec;
    //  additional_info += dec->additional_info;
}

BaseDecorator::BaseDecorator(Participant *, QString)
{

}


BaseDecorator::BaseDecorator(Participant *BaseDecorator)
{
    part = BaseDecorator;
}
BaseDecorator::BaseDecorator()
{

}
