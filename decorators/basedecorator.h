#ifndef BASEDECORATOR_H
#define BASEDECORATOR_H

class Participant;
#include <abstractsportsman.h>
#include <participant.h>



class BaseDecorator : public Participant
{
protected:
    Participant *part;
    QString additional_info;
public:

    QString getresult();
    QString getInfo();
    QString setInfo(QString);


     int getplace();
     void setplace(int);

     QString getname();
     QString getcountry();
     void setimage(QImage);
     void setimage(QImage *);

     QString getstatus();
     void setstatus(QString);

     QString getSex();
     unsigned int getAge();
     QImage *getImage();

    Participant *clone();
    BaseDecorator(BaseDecorator *);
    BaseDecorator(Participant *, QString);
    BaseDecorator(Participant *);
    BaseDecorator();
};



#endif // BASEDECORATOR_H
