#include "injurydecorator.h"

QString InjuryDecorator::getstatus()
{
//    return status + getInfo();
    return part->getstatus() + getInfo();
}

QString InjuryDecorator::getInfo()
{
    return additional_info;
}

InjuryDecorator::InjuryDecorator(Participant *s, QString str) : BaseDecorator(s, str){}

Participant *InjuryDecorator::clone()
{
    return new InjuryDecorator(this);
}

InjuryDecorator::InjuryDecorator(InjuryDecorator *dope) : BaseDecorator(dope)
{

}

InjuryDecorator::InjuryDecorator(Participant *participant)
{

    part = participant;
    additional_info = " травма";
}
