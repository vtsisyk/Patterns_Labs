#ifndef INJURYDECORATOR_H
#define INJURYDECORATOR_H

#include "basedecorator.h"



class InjuryDecorator : public BaseDecorator
{
public:
    QString getstatus();
    QString getInfo();
    InjuryDecorator(Participant *, QString );
    Participant *clone();
    InjuryDecorator(InjuryDecorator *);
    InjuryDecorator(Participant *);

};

#endif // INJURYDECORATOR_H
