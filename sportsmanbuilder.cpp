#include "sportsmanbuilder.h"

SportsmanBuilder::SportsmanBuilder()
{

}
void SportsmanBuilder::createSportsman(QString name, QString country, int age, QString _sex )
{
    sportsman = new SportsmanProxy(name, country, age, _sex);
    sportsman->setimage(defaultimgs.getUserpic());

}

void SportsmanBuilder::createNullSportsman()
{
    sportsman = NullSportsman::Instance();
}

void SportsmanBuilder::setimage(QImage img)
{
    sportsman->setimage(img);
}

AbstractSportsman *SportsmanBuilder::getSportsman()
{
    return sportsman;
}
