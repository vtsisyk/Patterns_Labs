#include "defaultimagespool.h"



void DefaultImagesPool::setUserpic(QImage img)
{
    userpic = img;
}

QImage *DefaultImagesPool::getUserpic()
{
    return &userpic;
}

void DefaultImagesPool::setSkipic(QIcon img)
{
    skipic = img;
}

QIcon *DefaultImagesPool::getSkipic()
{
    return &skipic;
}

QIcon *DefaultImagesPool::getRings()
{
    return &rings;
}

QIcon *DefaultImagesPool::sportIcon(QString img)
{
    if(img == "лыжи"){
        return getSkipic();
    } else if(img == "бег") {
        return getRings();
    } else if(img == "плавание") {
        return getRings();
    }else if(img == "хоккей") {
        return getRings();
    }else if(img == "фигурное катание") {
        return getRings();
    }else{
        return getRings();
    }
}

DefaultImagesPool::DefaultImagesPool()
{
    skipic = QIcon(":/resources/svg/ski.png");
    rings = QIcon(":/resources/svg/rings.png");
    userpic =  QImage(":/resources/svg/fans/stick_figure1.png");
}

