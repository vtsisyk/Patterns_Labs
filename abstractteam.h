#ifndef ABSTRACTTEAM_H
#define ABSTRACTTEAM_H

#include "participant.h"



class AbstractTeam
{
public:
    virtual void addParticipant(Participant *) = 0;
    virtual int getplace() = 0;
    virtual void setplace(int) = 0;
    virtual QVector<Participant *> getteam() = 0;
    virtual QString getcountry() = 0;

    virtual AbstractTeam *clone() = 0;
    virtual bool inTeam(AbstractSportsman *) = 0;
    virtual QSet<Participant *> listParticipants() = 0;

    static bool compare(AbstractTeam *a, AbstractTeam *b){
        if(a->getplace() > b->getplace())
            return true;
        return false;
    }
};

#endif // ABSTRACTTEAM_H
