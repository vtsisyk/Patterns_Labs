#ifndef ABSTRACTSPORT_H
#define ABSTRACTSPORT_H

#include "abstractcompetition.h"

#include <QIcon>
#include <QMap>

class Olympics;

class AbstractSport
{
public:
  //  AbstractSport();
    virtual QVector<AbstractCompetition *> getCompetitions() = 0;
    virtual QString getTitle() = 0;
    virtual void setTitle(QString) = 0;
    virtual QIcon *getImage() = 0;
    virtual void addCompetition(AbstractCompetition *) = 0;
    virtual AbstractSport *clone() =0;
    virtual void setImage(QIcon *) = 0;

    virtual QSet<Participant *> listParticipants() = 0;
    virtual bool sportsmanExists(AbstractSportsman *) = 0;
    virtual QMap<QString, countryinfo > personsByCountry() = 0;

    virtual void rmCompetition(AbstractCompetition *) = 0;

    virtual ~AbstractSport(){}
};

#endif // ABSTRACTSPORT_H
