#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "abstractprinter.h"
#include "section.h"
#include "sportsmanform.h"

#include <QGraphicsScene>
#include <QMainWindow>
#include <QModelIndex>
#include <QPropertyAnimation>
#include <QVBoxLayout>
#include <qstandarditemmodel.h>


#include "piechart/custompiechart.h"
#include "piechart/customslice.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCore/QRandomGenerator>
#include <QtCharts/QChartView>
#include <QtCharts/QLegend>
#include <QtCharts/QPieSeries>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QStackedBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QBarSeries>
#include <QValueAxis>
#include <editui/sportsform.h>
#include <simulator/machine.h>

QT_CHARTS_USE_NAMESPACE
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public AbstractPrinter
{
    Q_OBJECT

private:
    void clear();
    QChartView *upper ;
    QChartView *lower ;
    QScrollArea *scroll;
    Ui::MainWindow *ui;

    QStandardItemModel *sportmodel;
    QStandardItemModel *compmodel;
    int sports = 0;
    int competition = 0;

    void printCompetitions(int);

     Section* section;
     SportsForm *sportform;
     Machine *machine;

    void drawPieChart(QMap<QString, countryinfo> info, QString title1, QString title2, QVBoxLayout *layout);
    void drawBarChart(QMap<QString, countryinfo> info, QString title, QVBoxLayout *layout);

public:

        void addMachine(Machine *);
        void notify();
        explicit MainWindow(AbstractOlympics *, QWidget *parent = 0);
    ~MainWindow();

public slots:
            void printSports();
    void addSport();
    void rmSport();

    void rmCompetition();
    void addCompetition();

    void selectedSport(QModelIndex);
    void selectedCompetition(QModelIndex);
    void save();


};

#endif // MAINWINDOW_H
