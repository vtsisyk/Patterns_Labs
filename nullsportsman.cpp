#include "defaultimagespool.h"
#include "nullsportsman.h"

NullSportsman *NullSportsman::Instance()
{
    static NullSportsman s;
    return &s;
}

NullSportsman::NullSportsman() : Sportsman("blank", "blank", 0, "blank")
{

    setimage(defaultimgs.getUserpic());
}

NullSportsman::~NullSportsman()
{

}
