#include "competitionfactory.h"
#include "individualcompetition.h"
#include "olympicsbuilder.h"
#include "sportfactory.h"
#include "sportsmanbuilder.h"
#include "summerparticipantfactory.h"
#include "winterparticipantfactory.h"

OlympicsBuilder::OlympicsBuilder()
{
}


void OlympicsBuilder::createOlympics(QString name, int year, QString type)
{
    games = new Olympics(name, year, type);
}

Olympics *OlympicsBuilder::getOlympics()
{
    return games;
}
