#ifndef INDIVIDUALCOMPETITION_H
#define INDIVIDUALCOMPETITION_H


#include "abstractcompetition.h"
#include "participant.h"
#include "logger.h"
#include "sportsman.h"

#include <QVector>



class IndividualCompetition : public AbstractCompetition
{
    QVector<Participant *> participants;
public:    
    void addParticipant(Participant *);
    IndividualCompetition(QString);
    IndividualCompetition(QString, QDateTime, QDateTime);

    IndividualCompetition(IndividualCompetition *);
    AbstractCompetition *clone();
    bool isDeletable(AbstractSportsman *);

    QSet<Participant *> listParticipants();

     QVector<Participant *> getWinners();
     void rmParticipant(Participant *);
};



#endif // INDIVIDUALCOMPETITION_H
