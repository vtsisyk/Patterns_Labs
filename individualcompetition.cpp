#include "individualcompetition.h"

#include <QSet>
#include <QtAlgorithms>

void IndividualCompetition::addParticipant(Participant *person)
{
    participants.push_back(person);

}

IndividualCompetition::IndividualCompetition(QString _title)
{
    title = _title;
}

IndividualCompetition::IndividualCompetition(QString name, QDateTime start, QDateTime end)
{
    title = name;
    setstartTime(end);
    setendTime(end);
}

IndividualCompetition::IndividualCompetition(IndividualCompetition *comp)
{
    title = comp->title;
    timeBegin = comp->timeBegin;
    timeEnd = comp->timeEnd;

    for(Participant *part: comp->participants){
        participants.push_back(part->clone());
    }

}

AbstractCompetition *IndividualCompetition::clone()
{
    return new IndividualCompetition(this);
}

bool IndividualCompetition::isDeletable(AbstractSportsman *sportsman)
{
    bool ret = false;
    for(Participant *part: participants){
        ret = part->isThatYou(sportsman);
        if(ret)
            break;
    }
    return ret;
}

QSet<Participant *> IndividualCompetition::listParticipants()
{
    QSet<Participant *> sportsmans;
    for(Participant *part: participants)
        sportsmans.insert(part);
    return sportsmans;
}

QVector<Participant *> IndividualCompetition::getWinners()
{
    QVector<Participant *> tmp;
    qSort(participants.begin(), participants.end(), Participant::compare);

    for(Participant *someone: participants){
        if(someone->getplace() >= 1 && someone->getplace() <=3 )
            tmp.push_back(someone);
        if(someone->getplace() > 3)
            break;
    }
    return tmp;
}

void IndividualCompetition::rmParticipant(Participant *part)
{
    for(int i = 0 ; i < participants.size();i++)
        if(participants[i] == part){
            participants.remove(i);
            break;
        }
}




