#include "participant.h"

Participant::Participant()
{
}

int Participant::getplace()
{
    return place;
}

void Participant::setplace(int _place)
{
    place = _place;
}

QString Participant::getname()
{
    return sportsman->getname();
}



QString Participant::getcountry()
{
    return sportsman->getcountry();
}

void Participant::setimage(QImage img)
{
    if(sportsman != nullptr)
        sportsman->setimage(img);
}

void Participant::setimage(QImage *img)
{
    if(img != nullptr)
        sportsman->setimage(img);
}

QString Participant::getstatus()
{
    return status;
}

void Participant::setstatus(QString stat)
{
    status = stat;
}

QString Participant::getSex()
{
    return sportsman->getSex();
}

unsigned int Participant::getAge()
{
   return sportsman->getAge();
}

QImage *Participant::getImage()
{
   return sportsman->getImage();
}

bool Participant::isThatYou(AbstractSportsman *person)
{
    if(sportsman == person)
        return true;
    else
        return false;
}

AbstractSportsman *Participant::person()
{
    return sportsman;
}

bool Participant::compare(Participant *a, Participant *b)
{
    if(a->place < b->place)
        return true;
    return false;
}

Participant::~Participant()
{

}


//Participant::Participant(AbstractSportsman *_sportsman)
//{
//    sportsman = _sportsman;
//}
