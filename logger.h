#ifndef LOGGER_H
#define LOGGER_H

#include <QDateTime>
#include <QString>
#include <iostream>
#include <fstream>

class Logger
{
    std::streambuf * buf;
    std::ofstream of;
    std::ostream *out;
    QDateTime datetime;
public:
    static Logger& Instance();
    void log(QString);
    void setOutput(QString);

private:
  Logger() { setOutput("stdout"); }  // конструктор недоступен
  ~Logger() {  } // и деструктор

  /* TODO: вывод в файл? */
  // необходимо также запретить копирование
  Logger(Logger const&) = delete;
  Logger& operator= (Logger const&) = delete;
};


#endif // LOGGER_H
