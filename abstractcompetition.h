#ifndef ABSTRACTCOMPETITION_H
#define ABSTRACTCOMPETITION_H

#include "participant.h"
class Sport;
#include <QDateTime>

typedef struct countryinfo_t{
    int participants = 0;
    int gold = 0;
    int silver = 0;
    int bronze = 0;
} countryinfo;

class AbstractCompetition
{
protected:
    QDateTime timeBegin;
    QDateTime timeEnd;
    QString title;

public:

    QDateTime getstartTime();
    QDateTime getendTime();

    void setstartTime(QDateTime);
    void setendTime(QDateTime);

    QString getTitle();
        virtual AbstractCompetition *clone() = 0;
    virtual QVector<Participant *> getWinners() = 0;

    virtual QSet<Participant *> listParticipants() = 0;
    virtual bool isDeletable(AbstractSportsman *) = 0;
    virtual void rmParticipant(Participant *) = 0;


};

#endif // ABSTRACTCOMPETITION_H
