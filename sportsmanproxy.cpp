#include "nullsportsman.h"
#include "sportsmanproxy.h"

/* TODO: чекнуть, что он допущен к соревнованиям */
SportsmanProxy::SportsmanProxy(QString name,
                               QString country,
                               unsigned int age,
                               QString sex)
{
    /* TODO: сделай нормальную чекалку */

    if(name == ""){
        sportsman = NullSportsman::Instance();
        return;
    }
    /* проверить в списке стран? */
    if(country == ""){
        sportsman = NullSportsman::Instance();
        return;
    }

    if(!(sex == "M" || sex == "F")){
        sportsman = NullSportsman::Instance();
        return;

    }

    if((age < 15 || age > 70)){
        sportsman = NullSportsman::Instance();
        return;
    }

    sportsman = new Sportsman(name, country, age, sex);

}


QString SportsmanProxy::getname()
{
    return sportsman->getname();
}

QString SportsmanProxy::getcountry()
{
    return sportsman->getcountry();
}

void SportsmanProxy::setimage(QImage img)
{
    sportsman->setimage(img);
}

void SportsmanProxy::setimage(QImage *img)
{
    if(img != nullptr)
        sportsman->setimage(img);
}


QString SportsmanProxy::getSex()
{
    return sportsman->getSex();
}

unsigned int SportsmanProxy::getAge()
{
    return sportsman->getAge();
}

QImage *SportsmanProxy::getImage()
{
    return sportsman->getImage();
}

SportsmanProxy::~SportsmanProxy()
{
    if(sportsman != nullptr)
        sportsman->~AbstractSportsman();
}
