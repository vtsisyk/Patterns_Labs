#include "collectivecompetition.h"
#include "competitionfactory.h"
#include "individualcompetition.h"


CollectiveCompetition *CompetitionFactory::createCollective(QString title)
{
    return new CollectiveCompetition(title);
}

IndividualCompetition *CompetitionFactory::createIndividual(QString title)
{
    return new IndividualCompetition(title);
}


CompetitionFactory::CompetitionFactory()
{

}
