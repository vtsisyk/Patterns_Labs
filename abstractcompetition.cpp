#include "abstractcompetition.h"

QDateTime AbstractCompetition::getstartTime()
{
    return timeBegin;
}

QDateTime AbstractCompetition::getendTime()
{
    return timeEnd;
}

void AbstractCompetition::setstartTime(QDateTime time)
{
    timeBegin = time;
}

void AbstractCompetition::setendTime(QDateTime time)
{
    timeEnd = time;
}


QString AbstractCompetition::getTitle()
{
    return title;
}

