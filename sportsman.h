#ifndef SPORTSMAN_H
#define SPORTSMAN_H

#include "abstractsportsman.h"
#include "participant.h"


#include <QImage>
#include <QString>


class Sportsman : public AbstractSportsman
{
private:
    bool custompic;
protected:
    /* базовая информация о человеке */
    QString name;
    QString country;
    QString sex;
    unsigned int age;
    QImage *image;


    Sportsman();
public:
    QString getname();
    QString getcountry();

    QString getSex();
    unsigned int getAge();
    QImage *getImage();


    void setimage(QImage);
    void setimage(QImage *);

    Sportsman(QString, QString, unsigned int, QString);

    ~Sportsman();

};


#endif // SPORTSMAN_H
