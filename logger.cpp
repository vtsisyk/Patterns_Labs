#include "logger.h"
Logger& Logger::Instance()
{
    // согласно стандарту, этот код ленивый и потокобезопасный
    static Logger s;
    return s;
}
void Logger::log(QString action)
{
   *out << datetime.currentDateTime().toString("dd/MM/yyyy (HH:mm:ss)").toUtf8().constData()
             << "  "
             << action.toUtf8().constData()
             << "\n";
}

void Logger::setOutput(QString file)
{
    if(file != "stdout") {
        /* TODO: чекнуть на то, что файл можно открыть */
        /* TODO: закрыть файл если мы пишем в другой */
        of.open(file.toUtf8().constData());
        buf = of.rdbuf();
    } else {
        buf = std::cout.rdbuf();
    }

    out = new std::ostream(buf);
}
