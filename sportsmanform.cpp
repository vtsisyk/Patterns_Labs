#include "sportsmanform.h"
#include "ui_sportsmanform.h"

Participant *SportsmanForm::getParticipant()
{
    return participant;
}

SportsmanForm::SportsmanForm(Participant *part, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SportsmanForm)
{
    participant = part;
    ui->setupUi(this);
    ui->imgLabel->setPixmap(QPixmap::fromImage(*participant->getImage()).scaledToHeight(100));
    int place = participant->getplace();
    if(place == 1){
        ui->placeLabel->setText("Победитель");
        ui->widget->setStyleSheet("background-color: #ffd700;\
                                  opacity: 0.3;  border-radius: 6px; margin: 8px;");

    } else if(place == 2) {
        ui->placeLabel->setText("Призер");
        ui->widget->setStyleSheet("background-color: #c0c0c0;\
                                  opacity: 0.3;  border-radius: 6px; margin: 8px;");

    }else if(place == 3) {
        ui->placeLabel->setText("Призер");
        ui->widget->setStyleSheet("background-color: #CD7F32;\
                                  opacity: 0.3;  border-radius: 6px; margin: 8px;");

    } else {
        ui->placeLabel->setText("Призер");
        ui->widget->setStyleSheet("background-color: #084FDD;\
                                  opacity: 0.3;  border-radius: 6px; margin: 8px;");
    }

    if(participant->getstatus().contains("допинг")){

        ui->widget->setStyleSheet("background-color: #d9f442 ;\
                                  opacity: 0.3;  border-radius: 6px; margin: 8px;");
    } else if(participant->getstatus().contains("травма")){
        ui->widget->setStyleSheet("background-color: #FF4500;\
                                  opacity: 0.3;  border-radius: 6px; margin: 8px;");

    }

            ui->nameText->setText(participant->getname());
            ui->countryText->setText(participant->getcountry());
            ui->ageText->setText(QString::number(participant->getAge()));
            ui->sexText->setText(participant->getSex());
            ui->statusText->setText(participant->getstatus());

            ui->widget->resize(400,400);

            connect(ui->deleteButton, SIGNAL(clicked(bool)), this, SLOT(deleteButton()));

}

SportsmanForm::~SportsmanForm()
{
    delete ui;
}

void SportsmanForm::deleteButton()
{
    emit removeThis(this);
}
