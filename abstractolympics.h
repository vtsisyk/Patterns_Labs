#ifndef ABSTRACTOLYMPICS_H
#define ABSTRACTOLYMPICS_H

class AbstractPrinter;
#include "abstractprinter.h"
#include "abstractsport.h"


#include <QMap>
#include <QString>





class AbstractOlympics
{
public:
    virtual QString getName() = 0;
    virtual QString getType() = 0;

    virtual void addPerson(AbstractSportsman *) = 0;
    virtual void rmSportsman(AbstractSportsman *) = 0;
    virtual void addPrinter(AbstractPrinter *) = 0;
    virtual void rmPrinter(AbstractPrinter *) = 0;
    virtual void rmLastPrinter() = 0;
    virtual void addSport(AbstractSport *) = 0;
    virtual QVector<AbstractSport *> getSports() = 0;
    virtual int getYear() const = 0;
    virtual void setYear(int value) = 0;

    virtual void rmSport(AbstractSport *)= 0;


    virtual QMap<QString, countryinfo > personsByCountry() = 0;


    virtual void notify() = 0;
};

#endif // ABSTRACTOLYMPICS_H
