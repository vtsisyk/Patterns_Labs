#ifndef TEXTPRINTER_H
#define TEXTPRINTER_H

#include "abstractprinter.h"


#include <QDateTime>
#include <QString>
#include <iostream>
#include <fstream>
class TextPrinter : public AbstractPrinter
{
    std::streambuf *buf;
    std::ofstream of;
    std::ostream *out;
public:
    TextPrinter(AbstractOlympics *);
    void notify();
    void setOutFile(QString);
    TextPrinter(AbstractOlympics *games, QString file);

    void printOlympics();
    void printSports(AbstractSport *);
    void printCompetitions(AbstractCompetition *);
    void printParticipants(Participant *);
//    void print(QVector<Olympics *>);
//    void print(Olympics *);
//    void print(AbstractSport *);
//    void print(QVector<AbstractSport *>);
};

#endif // TEXTPRINTER_H
