#ifndef ABSTRACTPARTICIPANT_H
#define ABSTRACTPARTICIPANT_H

#include "abstractsportsman.h"
#include <QImage>
#include <QString>

class Participant : public AbstractSportsman
{
private:
protected:
    AbstractSportsman *sportsman;
    int place;
    QString status;
    Participant();
public:
    virtual int getplace();
    virtual void setplace(int);

    virtual QString getresult() = 0;
    virtual Participant *clone() = 0;

    virtual QString getname();
    virtual QString getcountry();
    virtual void setimage(QImage);
    virtual void setimage(QImage *);

    virtual QString getstatus();
    virtual void setstatus(QString);

    virtual QString getSex();
    virtual unsigned int getAge();
    virtual QImage *getImage();

    bool isThatYou(AbstractSportsman *);
    AbstractSportsman * person();

    static bool compare(Participant *a, Participant *b);

    ~Participant();


    // Participant(AbstractSportsman *);
};



#endif // ABSTRACTPARTICIPANT_H
