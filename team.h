#ifndef TEAM_H
#define TEAM_H


#include "participant.h"
#include "abstractteam.h"


#include <QVector>

#include <QDebug>

class Team : public AbstractTeam
{
    QVector<Participant *> team;
    QString country;
    int place;

     QVector<Participant *> *teamptr();
public:
    Team(QString);
    Team(QVector<Participant *> _team, QString _country);

    Team(Team *);
    void addParticipant(Participant *);
    QVector<Participant *> getteam();

    Team *clone();
    QString getcountry();
    bool inTeam(AbstractSportsman *);
    QSet<Participant *> listParticipants();




    int getplace();

    void setplace(int);
    class iterator
    {
        QVector<Participant *> *team;
        int i;

        public:
            iterator();
            inline iterator(Team *n) {team = n->teamptr(); i = 0;}
            inline iterator(iterator *it) {team = it->team; i = 0;}
            inline Participant * &operator*() const { return (*team)[i]; }
            inline  Participant * operator->() { return (*team)[i]; }

            inline bool operator!=(const iterator &o) const { return i != o.i; }
            inline bool operator<(const iterator& other) const { return i < other.i; }
            inline bool operator<=(const iterator& other) const { return i <= other.i; }
            inline bool operator>(const iterator& other) const { return i > other.i; }
            inline bool operator>=(const iterator& other) const { return i >= other.i; }

            inline iterator operator++(int) {++i; return *this; }


            iterator begin(){
                i = 0;
                return *this;
            }

            iterator end(){
                i = (*team).size();
                return *this;
            }

    };

    friend class iterator;

    inline iterator begin(){
        iterator beg(this);
        return beg.begin();
    }

    inline iterator end(){
        iterator beg(this);
        return beg.end();
    }

};



#endif // TEAM_H
