#ifndef COLLECTIVECOMPETITION_H
#define COLLECTIVECOMPETITION_H

#include "abstractcompetition.h"
#include "abstractteam.h"
#include "logger.h"
#include "sportsman.h"

#include <QVector>



class CollectiveCompetition : public AbstractCompetition
{
private:
    QVector<AbstractTeam *> teams;
    QString title;
public:
    void addTeam(AbstractTeam *);
    bool addMember(Participant *, QString country);
    CollectiveCompetition(QString);

    CollectiveCompetition(CollectiveCompetition*);
    AbstractCompetition *clone();
    bool isDeletable(AbstractSportsman *);
    QSet<Participant *> listParticipants();
    void rmParticipant(Participant *);

  QVector<Participant *> getWinners();


};



#endif // COLLECTIVECOMPETITION_H
