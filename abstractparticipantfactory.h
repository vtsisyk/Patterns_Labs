#ifndef ABSTRACTPARTICIPANTFACTORY_H
#define ABSTRACTPARTICIPANTFACTORY_H

#include "participant.h"



class AbstractParticipantFactory
{
private:
    Participant *sportsman;
public:
    virtual void createParticipant(AbstractSportsman *, QString) = 0;
    virtual Participant *getParticipant() = 0;
    virtual Participant *setStatus(QString) = 0;
    virtual void dope() = 0;
    virtual void injury() = 0;

};

#endif // ABSTRACTPARTICIPANTFACTORY_H
