#include "sportsmanpool.h"

SportsmanPool::SportsmanPool()
{

}

void SportsmanPool::addSportsman(AbstractSportsman *sportsman)
{
    persons.insert(sportsman);
}

void SportsmanPool::rmSportsman(AbstractSportsman *sportsman)
{
    persons.remove(sportsman);
}

QSet<AbstractSportsman *> SportsmanPool::getSportsmans()
{
    return persons;
}

