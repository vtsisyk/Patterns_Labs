#include "team.h"

Team::Team(QString _country)
{
    country = _country;
}

Team::Team(QVector<Participant *> _team, QString _country)
{
    team = _team;
    country = _country;

}

Team::Team(Team * t)
{
   // team = t->country;
    country = t->country;
    place = t->place;

    for(Participant * part : t->team){
        addParticipant(part->clone());
    }
}

void Team::addParticipant(Participant *p)
{
    team.push_back(p);
}

QVector<Participant *> Team::getteam()
{
    return team;
}

Team *Team::clone()
{
    return new Team(this);
}

QVector<Participant *> *Team::teamptr()
{
    return &team;
}


QString Team::getcountry()
{
    return country;
}

bool Team::inTeam(AbstractSportsman *person)
{
    bool ret = false;
    for(Participant *p : team){
        ret = p->isThatYou(person);
        if(ret)
            break;
    }
    return ret;

}

QSet<Participant *> Team::listParticipants()
{
    QSet<Participant *> sportsmans;
     for(Participant *p : team)
        sportsmans.insert(p);
    return sportsmans;
}


int Team::getplace()
{
    return place;
}

void Team::setplace(int _place)
{
    place = _place;

    for(Participant *p : team)
        p->setplace(_place);
}
