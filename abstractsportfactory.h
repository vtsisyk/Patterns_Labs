#ifndef ABSTRACTSPORTFACTORY_H
#define ABSTRACTSPORTFACTORY_H

#include "abstractsport.h"



class AbstractSportFactory
{
public:
    virtual AbstractSport *createSport(QString) = 0;
};

#endif // ABSTRACTSPORTFACTORY_H
