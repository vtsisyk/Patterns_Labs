#ifndef OLYMPICSBUILDER_H
#define OLYMPICSBUILDER_H

#include "abstractcompetitionfactory.h"
#include "abstractolympicsbuilder.h"
#include "abstractparticipantfactory.h"
#include "abstractsportfactory.h"
#include "abstractsportsmanbuilder.h"
#include "olympics.h"

#include <QString>



class OlympicsBuilder : public AbstractOlympicsBuilder
{
private:
    Olympics *games;

public:
    OlympicsBuilder();

    void createOlympics(QString, int, QString);
    Olympics *getOlympics();
};

#endif // OLYMPICSBUILDER_H
