#ifndef ABSTRACTCOMPETITIONFACTORY_H
#define ABSTRACTCOMPETITIONFACTORY_H

#include "abstractcompetition.h"
#include "collectivecompetition.h"
#include "individualcompetition.h"

#include <QString>



class AbstractCompetitionFactory
{
public:
    virtual CollectiveCompetition *createCollective(QString) = 0;
    virtual IndividualCompetition *createIndividual(QString) = 0;
};

#endif // ABSTRACTCOMPETITIONFACTORY_H
