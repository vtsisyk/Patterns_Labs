#include "olympics.h"


int Olympics::getYear() const
{
    return year;
}

void Olympics::setYear(int value)
{
    year = value;
}



QMap<QString, countryinfo> Olympics::personsByCountry()
{

    QMap<QString, countryinfo> country;

    for(Participant *sp: listParticipants()){
        QString c = sp->getcountry();
        country[c].participants++;
        int place = sp->getplace();
        if(place == 1)
            country[c].gold++;
        if(place == 2)
            country[c].silver++;
        if(place == 3)
            country[c].bronze++;

    }
    return country;
}

void Olympics::notify()
{
    for(AbstractPrinter *p: printers)
        p->notify();
}

void Olympics::addPerson(AbstractSportsman * sportsman)
{
    pool.addSportsman(sportsman);
}

void Olympics::rmSportsman(AbstractSportsman *sportsman)
{
    if(!sportsmanExists(sportsman))
        pool.rmSportsman(sportsman);
}

void Olympics::rmSport(AbstractSport *s)
{

    for(int i = 0 ; i < sports.size();i++)
        if(sports[i] == s){
            sports.remove(i);
            break;
        }


}

bool Olympics::sportsmanExists(AbstractSportsman *sportsman)
{
    bool ret = false;
    for(AbstractSport *sport :sports){
        ret = sport->sportsmanExists(sportsman);
        if(ret)
            break;
    }
    return ret;
}

QSet<Participant *> Olympics::listParticipants()
{
    QSet<Participant *> sportsmans;
    for(AbstractSport *sport :sports)
        sportsmans.unite(sport->listParticipants());
    return sportsmans;
}

void Olympics::addPrinter(AbstractPrinter * _printer)
{
    printers.push_back(_printer);
}

void Olympics::rmPrinter(AbstractPrinter *_printer)
{
    printers.removeAll(_printer);
}

void Olympics::rmLastPrinter()
{
    printers.pop_back();
}

QString Olympics::getName()
{
    return name;
}

QString Olympics::getType()
{
    return type;
}

void Olympics::addSport(AbstractSport *sport)
{
    sports.push_back(sport);

}

QVector<AbstractSport *> Olympics::getSports()
{
    return sports;
}

Olympics::Olympics(QString _name, int _year, QString _type)
{
    name = _name;
    year = _year;
    type = _type;
}
