#ifndef WINTERPARTICIPANTFACTORY_H
#define WINTERPARTICIPANTFACTORY_H

#include "participant.h"
#include "abstractparticipantfactory.h"



class WinterParticipantFactory : public AbstractParticipantFactory
{
    Participant *p ;
public:
    WinterParticipantFactory();

     Participant *getParticipant();
     Participant *setStatus(QString);
     void dope();
     void injury();

    void createParticipant(AbstractSportsman *, QString);
    Participant *createSkier(AbstractSportsman *);
    Participant *createFigureSkater(AbstractSportsman *);
    Participant *createHockeyist(AbstractSportsman *);

};

#endif // WINTERPARTICIPANTFACTORY_H
