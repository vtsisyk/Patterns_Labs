#ifndef CONCRETESPORTSMANBUILDER_H
#define CONCRETESPORTSMANBUILDER_H


#include "abstractsportsmanbuilder.h"
#include "defaultimagespool.h"
#include "sportsmanproxy.h"

#include "nullsportsman.h"

class SportsmanBuilder : public AbstractSportsmanBuilder
{
private:
    AbstractSportsman *sportsman;
    DefaultImagesPool& defaultimgs = DefaultImagesPool::Instance();

public:
    SportsmanBuilder();
    void createSportsman(QString, QString, int, QString _sex);
    void createNullSportsman();
    void setimage(QImage);
    AbstractSportsman *getSportsman();
};



#endif // CONCRETESPORTSMANBUILDER_H
