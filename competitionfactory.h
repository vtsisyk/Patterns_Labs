#ifndef COMPETITIONFACTORY_H
#define COMPETITIONFACTORY_H

#include "abstractcompetition.h"
#include "abstractcompetitionfactory.h"



class CompetitionFactory : public AbstractCompetitionFactory
{
public:
    CollectiveCompetition *createCollective(QString);
    IndividualCompetition *createIndividual(QString);
    CompetitionFactory();
};

#endif // COMPETITIONFACTORY_H
