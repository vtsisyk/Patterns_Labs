#include "summerparticipantfactory.h"

#include <sports/runner.h>
#include <sports/swimmer.h>
#include <nullparticipant.h>
#include <decorators/dopedecorator.h>
#include <decorators/injurydecorator.h>
void SummerParticipantFactory::createParticipant(AbstractSportsman *_sportsman, QString _sport)
{
    if(_sport.toLower() == "бег"){
        p = createRunner(_sportsman);
    } else if(_sport.toLower() == "плавание"){
        p = createSwimmer(_sportsman);
    } else {
        p = NullParticipant::Instance();
    }
}

Participant *SummerParticipantFactory::getParticipant()
{
    return p;
}

Participant *SummerParticipantFactory::setStatus(QString status)
{
    p->setstatus(status);
}

void SummerParticipantFactory::dope()
{
    p= new DopeDecorator(p);


}

void SummerParticipantFactory::injury()
{
    p = new InjuryDecorator(p);


}

Participant *SummerParticipantFactory::createRunner(AbstractSportsman *_sportsman)
{
    return new Runner(_sportsman);
}

Participant *SummerParticipantFactory::createSwimmer(AbstractSportsman *_sportsman)
{
    return new Swimmer(_sportsman);
}

SummerParticipantFactory::SummerParticipantFactory()
{

}
