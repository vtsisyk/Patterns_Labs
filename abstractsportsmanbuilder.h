#ifndef ABSTRACTSPORTSMANBUILDER_H
#define ABSTRACTSPORTSMANBUILDER_H

#include "abstractsportsman.h"

#include <QString>



class AbstractSportsmanBuilder
{

public:
    virtual void createSportsman(QString, QString, int, QString) = 0;
    virtual void createNullSportsman() = 0;
    virtual AbstractSportsman *getSportsman() = 0;
    virtual void setimage(QImage) = 0;
};

#endif // ABSTRACTSPORTSMANBUILDER_H
