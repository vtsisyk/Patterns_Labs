#include "competitionfactory.h"
#include "defaultimagespool.h"
#include "mainwindow.h"
#include "olympicsbuilder.h"

#include "sportfactory.h"
#include "sportsmanbuilder.h"
#include "team.h"
#include "winterparticipantfactory.h"
#include <QApplication>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QDebug>
#include <sports/skier.h>
#include <simulator/machine.h>
#include <simulator/nextcommand.h>
#include <simulator/undocommand.h>
#include <decorators/dopedecorator.h>




int main(int argc, char *argv[])
{
    QApplication a(argc, argv);





    ///AbstractSimulator*  t = new StateSimulator();



    DefaultImagesPool& defaultimgs = DefaultImagesPool::Instance();

  //  defaultimgs.setUserpic(QImage(":/resources/svg/biathlon.png"));

    OlympicsBuilder *olympics = new OlympicsBuilder() ;
    SportFactory *sports = new SportFactory();
    CompetitionFactory *comp = new CompetitionFactory();
    WinterParticipantFactory *part = new WinterParticipantFactory();
    SportsmanBuilder *sportsman = new SportsmanBuilder() ;



    Sportsman *sp = new Sportsman("ewrwer", "werwer", 12, "M");

    Participant *tee;
    Skier *sk = new Skier(sp);
    DopeDecorator * dp = new DopeDecorator(sk);
    qDebug()<< sk->getcountry();

    qDebug()<< dp->getcountry();

    Machine sim(olympics, sports, comp, part, sportsman);
    AbstractCompetition *trtr;
    AbstractSport *sprt;
    AbstractOlympics *ol;
    NextCommand next(&sim);
    UndoCommand previous(&sim);
    next.execute();
    sim.createOlympics("Москва", 1980, "Зимние");
    next.execute();
    sim.createSport("лыжи");
    next.execute();
    sim.createCompetition("марафон на 50 км", "individual");
    next.execute();
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    tee = sim.getcurrentParticipant();
    tee->setimage(QImage(":/resources/svg/skeleton.png"));
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    tee = sim.getcurrentParticipant();
    tee->setimage(QImage(":/resources/svg/skeleton.png"));
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    tee = sim.getcurrentParticipant();
    tee->setimage(QImage(":/resources/svg/skeleton.png"));
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    tee = sim.getcurrentParticipant();
    tee->setimage(QImage(":/resources/svg/skeleton.png"));
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    previous.execute();
    sim.createCompetition("марафон на 50 км","individual");
    sprt = sim.getcurrentSport();
    ol = sim.getcurrentGames();
    ol->addSport(sprt->clone());


    next.execute();
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    next.execute();
    sim.createSport("фигурное катание");
    next.execute();
    sim.createCompetition("одиночное выступление","individual");
    next.execute();
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Brazil", 23, "F");
    sim.createSportsman("qweqwe", "Brazil", 23, "F");
    sim.createSportsman("qweqwe", "Ecuador", 23, "F");
    sim.createSportsman("qweqwe", "Aegypt", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Czech Republic", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Czech Republic", 23, "F");
    next.execute();
    sim.createSport("биатлон");
    next.execute();
    sim.createCompetition("гонка-преследование", "individual");
    next.execute();
    sim.createSportsman("qweqwe", "Finland", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Bahrain", 23, "F");
    sim.createSportsman("qweqwe", "Australia", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Armenia", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    previous.execute();
    sim.createCompetition("марафон на 25 км", "individual");
    next.execute();
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Poland", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
    sim.createSportsman("qweqwe", "Belgium", 23, "F");
    sim.createSportsman("qweqwe", "Australia", 23, "F");
    sim.createSportsman("qweqwe", "Greece", 23, "F");
    sim.createSportsman("qweqwe", "Norway", 23, "F");
    sim.createSportsman("qweqwe", "Greece", 23, "F");


    next.execute();

    AbstractOlympics *moskwa = sim.getOlympics();
    MainWindow *w = new MainWindow(moskwa);
    w->addMachine(&sim);
    moskwa->notify();




  //  Simulator& sim = Simulator::Instance();

//    sim.addBuilder(&test);








//    sim.createOlympics("Москва", 1980, "Зимние");
//    sim.createSport("лыжи");
//    sim.createCompetition("марафон на 50 км");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createCompetition("марафон на 50 км");
//    sim.createCompetition("марафон на 50 км");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSport("фигурное катание");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Czech Republic", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Czech Republic", 23, "F");
//    sim.createSport("биатлон");
//    sim.createCompetition("гонка-преследование");
//    sim.createSportsman("qweqwe", "Belgium", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Bahrain", 23, "F");
//    sim.createSportsman("qweqwe", "Australia", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createCompetition("марафон на 25 км");
//    sim.createCompetition("гонка-преследование");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Poland", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Belgium", 23, "F");
//    sim.createSportsman("qweqwe", "Australia", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createSportsman("qweqwe", "Norway", 23, "F");
//    sim.createSportsman("qweqwe", "Russian Federation", 23, "F");
//    sim.createCompetition("эстафета");
//    sim.createSport("хоккей");

//    sim.created()[0];

//    Olympics *moskwa = sim.created()[0];




//    MainWindow *w = new MainWindow(moskwa);
//        moskwa->notify();
////    sim.createOlympics("Лондон", 2012, "Зимние");

//     pr->print(sim.created());
// //    pr->print(test.getOlympics());*/
// //    pr->print(test.getOlympics()->getSports());



//    SportsmanBuilder test;
//    WinterParticipantFactory wint;
//    Team *t = new Team("Yougy");

//    test.createSportsman("werwer", "werwer", 20, "F");

//    Skier *part = new Skier(test.getSportsman());

//    test.createSportsman("weasffrrrrdasdasdwer", "werwer", 20, "F");
//    Skier *part2 =  new Skier(test.getSportsman());

//    t->addParticipant(part);
//    t->addParticipant(part2);
//    t->getcountry();


//    Team::iterator it(t);


//    for(it = t->begin(); it < t->end(); it++){
//        qDebug() << it->getname();

//    }












    return a.exec();
}
