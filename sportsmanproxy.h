#ifndef SPORTSMANPROXY_H
#define SPORTSMANPROXY_H

#include "sportsman.h"



class SportsmanProxy : public AbstractSportsman
{
    AbstractSportsman *sportsman;
public:
    SportsmanProxy(QString, QString , unsigned int, QString);
    QString getname();
    QString getcountry();
    void setimage(QImage);
    void setimage(QImage *);

    QString getSex();
    unsigned int getAge();
    QImage *getImage();
    ~SportsmanProxy();

};




#endif // SPORTSMANPROXY_H
