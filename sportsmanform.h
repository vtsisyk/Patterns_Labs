#ifndef SPORTSMANFORM_H
#define SPORTSMANFORM_H

#include "participant.h"

#include <QWidget>

namespace Ui {
class SportsmanForm;
}

class SportsmanForm : public QWidget
{
    Q_OBJECT

    Participant *participant;
public:
    Participant *getParticipant();
    explicit SportsmanForm(Participant *part, QWidget *parent = 0);
    ~SportsmanForm();
public slots:
    void deleteButton();
private:
    Ui::SportsmanForm *ui;
signals:
    void removeThis(SportsmanForm *);
};

#endif // SPORTSMANFORM_H
