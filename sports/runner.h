#ifndef RUNNER_H
#define RUNNER_H

#include <QDateTime>
#include <participant.h>



class Runner : public Participant
{
    QTime result;
public:
    void setresult(QTime);

    QString getresult();
    QTime getTime();
    Participant *clone() ;
public:
    Runner(AbstractSportsman *);
    Runner(AbstractSportsman *, QTime);
    Runner(Runner *);
};

#endif // RUNNER_H
