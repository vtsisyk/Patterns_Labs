#include "runner.h"

void Runner::setresult(QTime _result)
{
    result = _result;
}

QString Runner::getresult()
{
    return result.toString("mm:ss:ms");
}

QTime Runner::getTime()
{
    return result;
}

Participant *Runner::clone()
{
    return new Runner(this);
}

Runner::Runner(AbstractSportsman *_sportsman)
{
    sportsman = _sportsman;
}

Runner::Runner(AbstractSportsman * _sportsman, QTime _result)
{
    sportsman = _sportsman;
    result = _result;
}

Runner::Runner(Runner *Runner)
{
    sportsman = Runner->sportsman;
    result = Runner->result;
}

