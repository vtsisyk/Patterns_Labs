#ifndef FIGURESKATER_H
#define FIGURESKATER_H

#include <participant.h>




class FigureSkater : public Participant
{
private:
    double result;
    double penalties;
    double elements;
    double components;
public:
    void setPenalties(double);
    void setElements(double);
    void setComponents(double);

    double getPenalties();
    double getElements();
    double getComponents();

    void recalcResult();
    QString getresult();

    FigureSkater *clone();
    FigureSkater(AbstractSportsman *);
    FigureSkater(FigureSkater *);
    FigureSkater(AbstractSportsman *, double, double, double);
};

#endif // FIGURESKATER_H
