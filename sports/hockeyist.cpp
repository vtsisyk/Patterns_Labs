#include "hockeyist.h"



QTime Hockeyist::getTimePlayed()
{
    return playedTime;
}

void Hockeyist::getTimePlayed(QTime time)
{
    playedTime = time;
}

int Hockeyist::getgoals()
{
    return goals;
}

void Hockeyist::setgoals(int _goals)
{
    goals = _goals;
}

QTime Hockeyist::getPenaltyTime()
{
    return penaltyTime;
}

void Hockeyist::setPenaltyTime(QTime time)
{
    penaltyTime = time;
}

QString Hockeyist::getRole()
{
    return role;
}

void Hockeyist::setRole(QString _role)
{
    role = _role;
}

QString Hockeyist::getresult()
{
    return role + QString::fromUtf8(", ") +
           QString::fromUtf8("Забил: ") +
           QString::number(goals) +
           QString::fromUtf8(" Играл: ") +
           playedTime.toString("mm:ss") +
           QString::fromUtf8(" штрафное время: ") +
           penaltyTime.toString("mm:ss");

}

Participant *Hockeyist::clone()
{
    return new Hockeyist(this);
}

Hockeyist::Hockeyist(AbstractSportsman * _sportsman)
{
    sportsman = _sportsman;
}

Hockeyist::Hockeyist(AbstractSportsman *_sportsman,
                     QTime _timePlayed,
                     int _goals,
                     QTime _penaltyTime,
                     QString _role)
{
    sportsman = _sportsman;
    playedTime = _timePlayed;
    goals  =_goals;
    penaltyTime = _penaltyTime;
    role = _role;
}

Hockeyist::Hockeyist(Hockeyist *hockeyist)
{

    sportsman = hockeyist->sportsman;
    playedTime = hockeyist->playedTime;
    goals  = hockeyist->goals;
    penaltyTime = hockeyist->penaltyTime;
    role = hockeyist->role;
}
