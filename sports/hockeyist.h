#ifndef HOCKEYIST_H
#define HOCKEYIST_H

#include <QDateTime>
#include <participant.h>



class Hockeyist : public Participant
{
    QTime playedTime;
    int goals;
    QTime penaltyTime;
    QString role;

public:

    QTime getTimePlayed();
    void getTimePlayed(QTime);

    int getgoals();
    void setgoals(int);

    QTime getPenaltyTime();
    void setPenaltyTime(QTime);

    QString getRole();
    void setRole(QString);

    QString getresult();
    Participant *clone() ;

    Hockeyist(AbstractSportsman *);
    Hockeyist(AbstractSportsman *, QTime, int, QTime, QString);
    Hockeyist(Hockeyist *);
};

#endif // HOCKEYIST_H
