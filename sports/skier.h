#ifndef SKIER_H
#define SKIER_H

#include <QDateTime>
#include <participant.h>



class Skier : public Participant
{
private:
    QTime result;
    double distance;
public:
    double getDistance();
    void setDistance(double);

    void setresult(QTime);
    QString getresult();
    Skier(AbstractSportsman *);
    Skier(Skier *);
    Skier *clone();


};

#endif // SKIER_H
