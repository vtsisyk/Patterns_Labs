#include "swimmer.h"

void Swimmer::setresult(QTime _result)
{
    result = _result;
}

void Swimmer::setLaps(int _laps)
{
    laps = _laps;
}

int Swimmer::getLaps()
{
    return laps;
}

QString Swimmer::getresult()
{
    return result.toString("mm:ss:ms") + "круги: " + QString::number(laps);
}

QTime Swimmer::getTime()
{
    return result;
}

Participant *Swimmer::clone()
{
    return new Swimmer(this);
}

Swimmer::Swimmer(AbstractSportsman *_sportsman)
{
    sportsman = _sportsman;
}

Swimmer::Swimmer(AbstractSportsman * _sportsman, QTime _result, int _laps)
{
    sportsman = _sportsman;
    result = _result;
    laps = _laps;
}

Swimmer::Swimmer(Swimmer *swimmer)
{
    sportsman = swimmer->sportsman;
    result = swimmer->result;
    laps = swimmer->laps;
}

