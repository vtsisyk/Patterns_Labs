#include "skier.h"



double Skier::getDistance()
{
    return distance;
}

void Skier::setDistance(double dist)
{
    distance = dist;
}

void Skier::setresult(QTime time)
{
    result = time;
}

QString Skier::getresult()
{
    return result.toString("HH:mm:ss:ms");
}

Skier::Skier(AbstractSportsman *_sportsman)
{
    sportsman = _sportsman;
}

Skier::Skier(Skier *ski)
{
    sportsman =  ski->sportsman;
    result = ski->result;
    distance = ski->distance;
}

Skier *Skier::clone()
{
    return new Skier(this);
}


