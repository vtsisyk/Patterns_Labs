#ifndef SWIMMER_H
#define SWIMMER_H

#include <QDateTime>
#include <participant.h>



class Swimmer : public Participant
{
    QTime result;
    int laps;
public:
    void setresult(QTime);
    void setLaps(int);
    int getLaps();

    QString getresult();
    QTime getTime();
    Participant *clone() ;

    Swimmer(AbstractSportsman *);
    Swimmer(AbstractSportsman *, QTime, int);
    Swimmer(Swimmer *);

};

#endif // SWIMMER_H
