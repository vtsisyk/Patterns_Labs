#include "figureskater.h"

void FigureSkater::setPenalties(double penalty)
{
    penalties = penalty;
    recalcResult();
}

void FigureSkater::setElements(double elem)
{
    elements = elem;
    recalcResult();
}

void FigureSkater::setComponents(double comp)
{
    components = comp;

    recalcResult();
}

double FigureSkater::getPenalties()
{
    return penalties;
}

double FigureSkater::getElements()
{
    return elements;
}

double FigureSkater::getComponents()
{
    return components;
}

void FigureSkater::recalcResult()
{
    result = elements + components - penalties;
}

QString FigureSkater::getresult()
{
    return QString::number(result);
}

FigureSkater *FigureSkater::clone()
{
    return new FigureSkater(this);
}

FigureSkater::FigureSkater(AbstractSportsman *_sportsman)
{
    sportsman = _sportsman;
    penalties = elements = components = 0;
}

FigureSkater::FigureSkater(FigureSkater *fig)
{
    sportsman = fig->sportsman;
    penalties = fig->penalties;
    elements = fig->elements;
    components = fig->components;
}

FigureSkater::FigureSkater(AbstractSportsman *_sportsman,
                           double penalty,
                           double elem,
                           double comp)
{
    sportsman = _sportsman;
    setPenalties(penalty);
    setElements(elem);
    setComponents(comp);
}


