#ifndef SPORTSFORM_H
#define SPORTSFORM_H

#include <QStandardItemModel>
#include <QWidget>
#include <abstractsport.h>

namespace Ui {
class SportsForm;
}

class SportsForm : public QWidget
{
    Q_OBJECT

    QIcon  *img;
    AbstractSport *sport;

public:
    explicit SportsForm(QWidget *parent = 0);
    void setImage(QIcon);
    void setData(AbstractSport *s);

    ~SportsForm();

    void setText(QString text);
public slots:
    void changeIcon();
    void save();

private:
    Ui::SportsForm *ui;
};

#endif // SPORTSFORM_H
