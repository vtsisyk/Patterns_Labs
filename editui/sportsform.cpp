#include "sportsform.h"
#include "ui_sportsform.h"
#include <QFileDialog>

SportsForm::SportsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SportsForm)
{


    ui->setupUi(this);
     connect(ui->editButton, SIGNAL(clicked(bool)), this, SLOT(changeIcon()));
      connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));

        ui->imageLabel->setMinimumSize(140, 140);
        ui->imageLabel->setAlignment(Qt::AlignCenter);
        ui->imageLabel->setFrameShape(QFrame::Box);
        ui->imageLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        ui->imageLabel->setBackgroundRole(QPalette::Base);
        ui->imageLabel->setAutoFillBackground(true);
}

void SportsForm::setText(QString text)
{
    ui->lineEdit->setText(text);
}

void SportsForm::changeIcon()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Открыть"), "",
                                                    tr("Изображение (*.png);;All Files (*)"));

    if(!fileName.isEmpty()){
        img = new QIcon(fileName);
        setImage(*img);
    }
}

void SportsForm::save()
{
    sport->setImage(img);
    sport->setTitle(ui->lineEdit->text());
}

void SportsForm::setImage(QIcon img)
{
    QPixmap px = img.pixmap(100,100);
     ui->imageLabel->setPixmap(px);


  //   ui->imageLabel->setEnabled(false);


     update();

}

void SportsForm::setData(AbstractSport *s)
{
    sport = s;
    setImage(*s->getImage());
    setText(s->getTitle());

}

SportsForm::~SportsForm()
{
    delete ui;
}
