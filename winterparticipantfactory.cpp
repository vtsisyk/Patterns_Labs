#include "winterparticipantfactory.h"

#include <sports/figureskater.h>
#include <sports/hockeyist.h>
#include <sports/skier.h>
#include <nullparticipant.h>
#include <decorators/dopedecorator.h>
#include <decorators/injurydecorator.h>

WinterParticipantFactory::WinterParticipantFactory()
{

}

Participant *WinterParticipantFactory::getParticipant()
{
    return p;
}

Participant *WinterParticipantFactory::setStatus(QString status)
{
    p->setstatus(status);
}

void WinterParticipantFactory::dope()
{
    p = new DopeDecorator(p);

}

void WinterParticipantFactory::injury()
{
    p =  new InjuryDecorator(p);

}


void WinterParticipantFactory::createParticipant(AbstractSportsman *_sportsman,
                                                                 QString _sport)
{
    if(_sport.toLower() == "лыжи"){
        p =  createSkier(_sportsman);
    } else if(_sport.toLower() == "фигурное катание"){
        p = createFigureSkater(_sportsman);
    } else if(_sport.toLower() == "хоккей"){
        p = createHockeyist(_sportsman);
    } else {
        //return createSkier(_sportsman);
        p = NullParticipant::Instance();
    }
}

Participant *WinterParticipantFactory::createSkier(AbstractSportsman *_sportsman)
{
    return new Skier(_sportsman);
}

Participant *WinterParticipantFactory::createFigureSkater(AbstractSportsman * _sportsman)
{
    return new FigureSkater(_sportsman);
}

Participant *WinterParticipantFactory::createHockeyist(AbstractSportsman * _sportsman)
{
    return new Hockeyist(_sportsman);
}
