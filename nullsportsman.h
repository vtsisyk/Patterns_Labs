#ifndef NULLSPORTSMAN_H
#define NULLSPORTSMAN_H

#include "defaultimagespool.h"
#include "sportsman.h"



class NullSportsman : public Sportsman
{
public:
    static NullSportsman * Instance();

private:

   DefaultImagesPool& defaultimgs = DefaultImagesPool::Instance();
   NullSportsman();  // конструктор недоступен
  ~NullSportsman(); // и деструктор

  // необходимо также запретить копирование
  NullSportsman(NullSportsman const&); // реализация не нужна
  NullSportsman& operator= (NullSportsman const&);  // и тут
};

#endif // NULLSPORTSMAN_H
