#include "collectivecompetition.h"

#include <QSet>


void CollectiveCompetition::addTeam(AbstractTeam *t)
{
    teams.push_back(t);
}

bool CollectiveCompetition::addMember(Participant *p, QString country)
{
    for(AbstractTeam * team : teams){
        if(team->getcountry() == country){
            team->addParticipant(p);
        }
        return true;
    }
    return false;
}

CollectiveCompetition::CollectiveCompetition(QString _title)
{
    title = _title;
}

CollectiveCompetition::CollectiveCompetition(CollectiveCompetition *comp)
{
    title  = comp->title;

    for(AbstractTeam * t : comp->teams){
        addTeam(t->clone());
    }
}

AbstractCompetition *CollectiveCompetition::clone()
{
    return new CollectiveCompetition(this);
}

bool CollectiveCompetition::isDeletable(AbstractSportsman *person)
{
    bool ret = false;
    for(AbstractTeam * team : teams){
        ret = team->inTeam(person);
        if(ret)
            break;
    }
    return ret;
}

QSet<Participant *> CollectiveCompetition::listParticipants()
{

    QSet<Participant *> sportsmans;
    for(AbstractTeam * team : teams)
        sportsmans.unite(team->listParticipants());
    return sportsmans;

}

void CollectiveCompetition::rmParticipant(Participant *)
{

}

QVector<Participant *> CollectiveCompetition::getWinners()
{
    QVector<Participant *> tmp;
    qSort(teams.begin(), teams.end(), AbstractTeam::compare);

    for(AbstractTeam *t: teams){
            if(t->getplace() >= 1 && t->getplace() <= 3){
                QSet<Participant *> sportsmans = t->listParticipants();
                for(Participant *someone: sportsmans)
                    tmp.push_back(someone);
            }
            if(t->getplace() > 3)
                break;

    }
    return tmp;
}

