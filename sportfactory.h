#ifndef SPORTFACTORY_H
#define SPORTFACTORY_H

#include "abstractsportfactory.h"
#include "defaultimagespool.h"



class SportFactory : public AbstractSportFactory
{
    DefaultImagesPool& defaultimgs = DefaultImagesPool::Instance();
public:
    SportFactory();
    AbstractSport *createSport(QString);
};

#endif // SPORTFACTORY_H
