#ifndef NULLOLYMPICSBUILDER_H
#define NULLOLYMPICSBUILDER_H

#include "abstractolympicsbuilder.h"
#include "olympics.h"



class NullOlympicsBuilder : public AbstractOlympicsBuilder
{
public:
    NullOlympicsBuilder();
    void createOlympics(QString, int, QString);
    Olympics *getOlympics();
};

#endif // NULLOLYMPICSBUILDER_H
