#include "nullparticipant.h"
#include <nullsportsman.h>
NullParticipant *NullParticipant::Instance()
{
    static NullParticipant s;
    return &s;
}

NullParticipant::NullParticipant()
{
    sportsman = NullSportsman::Instance();
    place = 0;
}

NullParticipant::~NullParticipant()
{

}

QString NullParticipant::getresult()
{
    return QString::fromUtf8("Ничего");
}

Participant *NullParticipant::clone()
{
    return this;
}
