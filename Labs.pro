#-------------------------------------------------
#
# Project created by QtCreator 2018-01-04T21:43:59
#
#-------------------------------------------------

QT       += core gui svg  charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Labs
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    sportsman.cpp \
    sportsmanproxy.cpp \
    logger.cpp \
    collectivecompetition.cpp \
    individualcompetition.cpp \
    abstractsportsman.cpp \
    abstractsportsmanbuilder.cpp \
    abstractsport.cpp \
    abstractcompetition.cpp \
    team.cpp \
    abstractteam.cpp \
    sports/skier.cpp \
    sports/figureskater.cpp \
    sport.cpp \
    abstractcompetitionfactory.cpp \
    abstractparticipantfactory.cpp \
    winterparticipantfactory.cpp \
    summerparticipantfactory.cpp \
    competitionfactory.cpp \
    olympics.cpp \
    abstractolympicsbuilder.cpp \
    olympicsbuilder.cpp \
    nullolympicsbuilder.cpp \
    abstractsportfactory.cpp \
    sportfactory.cpp \
    abstractpool.cpp \
    defaultimagespool.cpp \
    decorators/basedecorator.cpp \
    decorators/injurydecorator.cpp \
    decorators/dopedecorator.cpp \
    abstractprinter.cpp \
    textprinter.cpp \
    nullsportsman.cpp \
    sportsmanbuilder.cpp \
    abstractolympics.cpp \
    participant.cpp \
    piechart/drilldownchart.cpp \
    piechart/drilldownslice.cpp \
    sports/hockeyist.cpp \
    sports/runner.cpp \
    sports/swimmer.cpp \
    nullparticipant.cpp \
    sportsmanpool.cpp \
    simulator/machine.cpp \
    simulator/simulatorstate.cpp \
    simulator/simulatorempty.cpp \
    simulator/simulatorolympics.cpp \
    simulator/simulatorsport.cpp \
    simulator/simulatorcompetition.cpp \
    simulator/simulatorparticipant.cpp \
    simulator/simulatorindividual.cpp \
    simulator/memento.cpp \
    simulator/abstractcommand.cpp \
    simulator/undocommand.cpp \
    simulator/nextcommand.cpp \
    section.cpp \
    editui/sportsform.cpp \
    sportsmanform.cpp

HEADERS  += mainwindow.h \
    sportsman.h \
    sportsmanproxy.h \
    logger.h \
    collectivecompetition.h \
    individualcompetition.h \
    abstractsportsman.h \
    abstractsportsmanbuilder.h \
    translation.h \
    abstractsport.h \
    abstractcompetition.h \
    team.h \
    abstractteam.h \
    sports/skier.h \
    sports/figureskater.h \
    sport.h \
    abstractcompetitionfactory.h \
    abstractparticipantfactory.h \
    winterparticipantfactory.h \
    summerparticipantfactory.h \
    competitionfactory.h \
    olympics.h \
    abstractolympicsbuilder.h \
    olympicsbuilder.h \
    nullolympicsbuilder.h \
    abstractsportfactory.h \
    sportfactory.h \
    abstractpool.h \
    defaultimagespool.h \
    decorators/basedecorator.h \
    decorators/injurydecorator.h \
    decorators/dopedecorator.h \
    abstractprinter.h \
    textprinter.h \
    nullsportsman.h \
    sportsmanbuilder.h \
    abstractolympics.h \
    participant.h \
    piechart/customslice.h \
    piechart/custompiechart.h \
    sports/hockeyist.h \
    sports/runner.h \
    sports/swimmer.h \
    nullparticipant.h \
    sportsmanpool.h \
    simulator/machine.h \
    simulator/simulatorstate.h \
    simulator/simulatorempty.h \
    simulator/simulatorolympics.h \
    simulator/simulatorsport.h \
    simulator/simulatorcompetition.h \
    simulator/simulatorparticipant.h \
    simulator/simulatorindividual.h \
    simulator/memento.h \
    simulator/abstractcommand.h \
    simulator/undocommand.h \
    simulator/nextcommand.h \
    section.h \
    editui/sportsform.h \
    sportsmanform.h

FORMS    += mainwindow.ui \
    editui/sportsform.ui \
    sportsmanform.ui

STATECHARTS +=

RESOURCES += \
    svgs.qrc

DISTFILES +=
