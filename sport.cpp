#include "sport.h"

#include <QSet>

Sport::Sport(QString _title, QIcon *_img)
{
    title = _title;
    img = _img;
}

Sport::Sport(Sport *sport)
{
    title = sport->title;
    img = sport->img;

    for(AbstractCompetition *comp: sport->competitions){
        addCompetition(comp->clone());
    }
}

QString Sport::getTitle()
{
    return title;
}

void Sport::setTitle(QString _title)
{
    title = _title;
}


QIcon *Sport::getImage()
{
    return img;
}

void Sport::setImage(QIcon *icon)
{
        img = icon;
}


void Sport::addCompetition(AbstractCompetition *competition)
{
    competitions.push_back(competition);
}

void Sport::rmCompetition(AbstractCompetition *compe)
{
    for(int i = 0 ; i < competitions.size();i++)
        if(competitions[i] == compe){
            competitions.remove(i);
            break;
        }
}

QVector<AbstractCompetition *> Sport::getCompetitions()
{
    return competitions;
}

bool Sport::sportsmanExists(AbstractSportsman *sportsman)
{
    bool ret = false;
    for(AbstractCompetition *comp: competitions){
        ret = comp->isDeletable(sportsman);
        if(ret)
            break;
    }
    return ret;
}

QSet<Participant *> Sport::listParticipants()
{
    QSet<Participant *> sportsmans;
    for(AbstractCompetition *comp: competitions)
        sportsmans.unite(comp->listParticipants());
    return sportsmans;
}

QMap<QString, countryinfo> Sport::personsByCountry()
{
    QMap<QString, countryinfo> country;

    for(Participant *sp: listParticipants()){
        QString c = sp->getcountry();
        country[c].participants++;
        int place = sp->getplace();
        if(place == 1)
            country[c].gold++;
        if(place == 2)
            country[c].silver++;
        if(place == 3)
            country[c].bronze++;

    }
    return country;
}

AbstractSport *Sport::clone()
{
    return new Sport(this);
}

Sport::~Sport()
{

}
