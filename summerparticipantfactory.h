#ifndef SUMMERPARTICIPANTFACTORY_H
#define SUMMERPARTICIPANTFACTORY_H

#include "participant.h"
#include "abstractparticipantfactory.h"



class SummerParticipantFactory : public AbstractParticipantFactory
{
    Participant *p ;
public:

    Participant *getParticipant();
    Participant *setStatus(QString);
    void dope();
    void injury();

    void createParticipant(AbstractSportsman *, QString);

    Participant *createRunner(AbstractSportsman *);
    Participant *createSwimmer(AbstractSportsman *);
    SummerParticipantFactory();
};

#endif // SUMMERPARTICIPANTFACTORY_H
