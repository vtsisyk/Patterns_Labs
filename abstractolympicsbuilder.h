#ifndef ABSTRACTOLYMPICSBUILDER_H
#define ABSTRACTOLYMPICSBUILDER_H

#include "olympics.h"

#include <QString>



class AbstractOlympicsBuilder
{
public:
    virtual void createOlympics(QString, int, QString) = 0;
    virtual Olympics *getOlympics() = 0;
};

#endif // ABSTRACTOLYMPICSBUILDER_H
