#ifndef ABSTRACTSPORTSMAN_H
#define ABSTRACTSPORTSMAN_H

#include <QImage>
#include <QString>

class AbstractSportsman
{
public:
    virtual QString getname() = 0;
    virtual QString getcountry() = 0;
    virtual void setimage(QImage){}
    virtual void setimage(QImage *){}

    virtual QString getSex() = 0;
    virtual unsigned int getAge() = 0;
    virtual QImage *getImage() = 0;


    virtual ~AbstractSportsman() = 0;


};

#endif // ABSTRACTSPORTSMAN_H.
